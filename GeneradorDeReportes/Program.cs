﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace GeneradorDeReportes
{
    class Program
    {
        static int IsInt(string Line)
        {
                bool flag = int.TryParse(Line, out int value);
                if (flag == true)
                {
                    return value;
                }
                else
                {
                    Console.WriteLine("\nFavor de ingresar un valor entero correcto, para sustituir a "+ Line);
                    Line = Console.ReadLine();
                    value = IsInt(Line);
                    return value;
                }
            
        }

        static bool IsArray(string Line)
        {
            bool flag = false;
            if (Line.Contains(","))
            {
                Console.WriteLine("\nSecuencia de caracteres");
                flag = true;
            }
            return flag;
        }

        static  List<int> GetNumbers(string Line)
        {
            List<int> numbers = new List<int>();
            string[] characters = Line.Split(',');
                
            foreach(string c in characters)
            {
                int n = IsInt(c);
                numbers.Add(n);
            }
            return numbers;

        }

        static List<int> Validate(string Line)
        {
            bool flag = IsArray(Line);
            List<int> numbers = new List<int>();
            if(flag)
            {
                numbers = GetNumbers(Line);
            }
            else{
             numbers.Add(  IsInt(Line) );
            }

            return numbers;
        }

        public static async void GenerateThread(int n)
        {
            Report report = new Report();
            switch (n)
            {
                case 1:
                    Console.WriteLine("Generando Reporte General.. ");
                    report.CreateShipmentsReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'ReporteGeneralEnvios' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 2:
                    Console.WriteLine("Generando Babby.. ");
                    report.CreateBabbyReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'ReporteBabby' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 3:
                    Console.WriteLine("Generando Berdico.. ");
                    report.CreateBerdicoReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'ReporteBerdico' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 4:
                    Console.WriteLine("Generando Reporte RodMay.. ");
                    report.CreateRodMayReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'ReporteRodMay' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 5:
                    Console.WriteLine("Generando Reporte Baul.. ");
                    report.CreateBaulReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'ReporteBaul' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 6:
                    Console.WriteLine("Generando Reporte Element.. ");
                    report.CreateElementReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'ReporteElement' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 7:
                    Console.WriteLine("Generando Reporte GameSmart.. ");
                    report.CreateGameSmartReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'ReporteGameSmart' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 8:
                    Console.WriteLine("Generando Reporte Jesy.. ");
                    report.CreateJesyReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'ReporteJesy' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 9:
                    Console.WriteLine("Generando Reporte Sotano.. ");
                    report.CreateSotanoReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'ReporteSotano' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 10:
                    Console.WriteLine("Generando Reporte Likkus.. ");
                    report.CreateLikkusReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'ReporteLikkus' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 11:
                    Console.WriteLine("Generando Reporte Osmag.. ");
                    report.CreateOsmagReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'ReporteOsmag' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 12:
                    Console.WriteLine("Generando Reporte Psicofarma.. ");
                    report.CreatePsicofarmaReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'ReportePsicofarma' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 13:
                    Console.WriteLine("Generando Reporte Raga.. ");
                    report.CreateRAGAReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'ReporteRAGA' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 14:
                    Console.WriteLine("Generando Reporte Samsonite.. ");
                    Console.WriteLine("Generando Samsonite.. ");
                    report.CreateSamsoniteReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'ReporteSamsonite' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 15:
                    Console.WriteLine("Generando Reporte Samsonite Retornos.. ");
                    report.CreateSamsoniteRetornosReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'SamsoniteRetornos' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 16:
                    Console.WriteLine("Generando Reporte TecPluss.. ");
                    report.CreateTecPlussReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'TecPluss' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 17:
                    Console.WriteLine("Generando Reporte Uffi.. ");
                    report.CreateUffiReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'ReporteUffi' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 18:
                    Console.WriteLine("Generando Reporte VCV.. ");
                    report.CreateVCVReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'ReporteVCV' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 19:
                    Console.WriteLine("Generando Reporte IPISA.. ");
                    report.CreateIpisaReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'ReporteIPISA' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 20:
                    Console.WriteLine("Generando Reporte Sonia.. ");
                    report.CreateSoniaALReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'Reporte_SONIA_ALEJANDRA_BRAUER' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 21:
                    Console.WriteLine("Generando Reporte Custom.. ");
                    report.CreateCustomReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'ReporteCustom' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
                case 22:
                    Console.WriteLine("Generando Reporte Pendulo.. ");
                    report.CreatePenduloReport();
                    Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                        "\nbajo el nombre de 'ReportePendulo' + la fecha del dia de hoy!" +
                        "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                    break;
            }
        }
        static float IsFloat(string Line)
        {
            bool flag = float.TryParse(Line, out float value);
            if (flag == true)
            {
                return value;
            }
            else
            {
                Console.WriteLine("\nFavor de ingresar un valor correcto!");
                Line = Console.ReadLine();
                value = IsFloat(Line);
                return value;
            }
        }
        static void Main(string[] args)
        {
            Report report = new Report();
            //List<int> options;
            for (; ; )
            {
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Clear();
                Console.WriteLine("Generador de Reportes OPS Version 1.9.4" +
                    "\n\nBienvenido, ingrese por favor la opcion para generar el reporte deseado o una secuencia de numeros separados por comas..." +
                    "\n\n1.Reporte general de envios (diario)" +
                    "\n2.Reporte Babby Summer (diario)" +
                    "\n3.Reporte Berdico (diario)" +
                    "\n4.Reporte Comercial Rod May (diario)" +
                    "\n5.Reporte El Baul de Cleo (diario)" +
                    "\n6.Reporte Element (diario)" +
                    "\n7.Reporte Game Smart (diario)" +
                    "\n8.Reporte Jesy Almaguer (diario)" +
                    "\n9.Reporte Libreria El Sotano (M)" +
                    "\n10.Reporte Likkus (diario)" +
                    "\n11.Reporte OSMAG (diario)" +
                    "\n12.Reporte Psicofarma (LMV)" +
                    "\n13.Reporte RAGA (diario)" +
                    "\n14.Reporte Samsonite (diario)" +
                    "\n15.Reporte SamsoniteRetornos (diario)" +
                    "\n16.Reporte TecPluss (diario)" +
                    "\n17.Reporte Uffi Dreams (diario)" +
                    "\n18.Reporte VCV (diario)" +
                    "\n19.Reporte IPISA " +
                    "\n20.Reporte Sofia Alejandra " +
                    "\n21.Reporte Custom (esporadico)" +
                    "\n22.Reporte Pendulo"+
                    "\n23.Reporte Estafeta"+
                    "\n24.Reporte Redpack" +
                    "\n25.Reporte DHL");

                //  options = Validate(Console.ReadLine());

                //Console.Clear();
                //Console.WriteLine(options[0]);

                //foreach(int n in options)
                //{
                //    Thread t = new Thread(() => GenerateThread(n));
                //}
                int n = IsInt(Console.ReadLine());
                switch (n)
                {
                    case 1:
                        Console.WriteLine("Generando Reporte General.. ");
                        report.CreateShipmentsReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReporteGeneralEnvios' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 2:
                        Console.WriteLine("Generando Babby.. ");
                        report.CreateBabbyReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReporteBabby' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 3:
                        Console.WriteLine("Generando Berdico.. ");
                        report.CreateBerdicoReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReporteBerdico' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 4:
                        Console.WriteLine("Generando Reporte RodMay.. ");
                        report.CreateRodMayReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReporteRodMay' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 5:
                        Console.WriteLine("Generando Reporte Baul.. ");
                        report.CreateBaulReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReporteBaul' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 6:
                        Console.WriteLine("Generando Reporte Element.. ");
                        report.CreateElementReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReporteElement' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 7:
                        Console.WriteLine("Generando Reporte GameSmart.. ");
                        report.CreateGameSmartReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReporteGameSmart' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 8:
                        Console.WriteLine("Generando Reporte Jesy.. ");
                        report.CreateJesyReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReporteJesy' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 9:
                        Console.WriteLine("Generando Reporte Sotano.. ");
                        report.CreateSotanoReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReporteSotano' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 10:
                        Console.WriteLine("Generando Reporte Likkus.. ");
                        report.CreateLikkusReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReporteLikkus' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 11:
                        Console.WriteLine("Generando Reporte Osmag.. ");
                        report.CreateOsmagReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReporteOsmag' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 12:
                        Console.WriteLine("Generando Reporte Psicofarma.. ");
                        report.CreatePsicofarmaReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReportePsicofarma' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 13:
                        Console.WriteLine("Generando Reporte Raga.. ");
                        report.CreateRAGAReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReporteRAGA' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 14:
                        Console.WriteLine("Generando Reporte Samsonite.. ");
                        Console.WriteLine("Generando Samsonite.. ");
                        report.CreateSamsoniteReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReporteSamsonite' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 15:
                        Console.WriteLine("Generando Reporte Samsonite Retornos.. ");
                        report.CreateSamsoniteRetornosReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'SamsoniteRetornos' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 16:
                        Console.WriteLine("Generando Reporte TecPluss.. ");
                        report.CreateTecPlussReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'TecPluss' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 17:
                        Console.WriteLine("Generando Reporte Uffi.. ");
                        report.CreateUffiReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReporteUffi' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 18:
                        Console.WriteLine("Generando Reporte VCV.. ");
                        report.CreateVCVReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReporteVCV' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 19:
                        Console.WriteLine("Generando Reporte IPISA.. ");
                        report.CreateIpisaReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReporteIPISA' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 20:
                        Console.WriteLine("Generando Reporte Sonia.. ");
                        report.CreateSoniaALReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'Reporte_SONIA_ALEJANDRA_BRAUER' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 21:
                        Console.WriteLine("Generando Reporte Custom.. ");
                        report.CreateCustomReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReporteCustom' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 22:
                        Console.WriteLine("Generando Reporte Pendulo.. ");
                        report.CreatePenduloReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReportePendulo' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 23:
                        Console.WriteLine("Generando Reporte Estafeta.. ");
                        report.CreateEstafetaReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReporteEstafeta' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 24:
                        Console.WriteLine("Generando Reporte Redpack.. ");
                        report.CreateRedpackReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReporteEstafeta' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;
                    case 25:
                        Console.WriteLine("Generando Reporte DHL.. ");
                        report.CreateDHLReport();
                        Console.WriteLine("Tu archivo fue generado, lo encontraras en la carpeta de documentos" +
                            "\nbajo el nombre de 'ReporteDHL' + la fecha del dia de hoy!" +
                            "\n\nPara volver al menu principal favor de presionar la tecla Y...");
                        while (Console.ReadKey(true).Key != ConsoleKey.Y)
                        {

                        }
                        break;


                }





            }
        }
    }
}
