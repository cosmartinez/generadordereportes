﻿using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Text;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;

namespace GeneradorDeReportes
{
    class Report
    {

        float height;
        float length;
        float securedValue;
        float weight;
        float width;
        int shipmentType;
        List<float> heightsList = new List<float>();
        List<float> lengthsList = new List<float>();
        List<float> securedValuesList = new List<float>();
        List<float> weightsList = new List<float>();
        List<float> widthsList = new List<float>();
        List<string> trackingsList = new List<string>();
        List<string> statusList = new List<string>();
        string content;
        string destinataryCity;
        string destinataryCompany;
        string destinataryCountry;
        string destinataryName;
        string destinataryState;
        string destinataryZipCode;
        //30/04/2021 -reporte alejandra brauem
        string destinaryContactName;
        string destinaryPhonenumber;
        string destinaryEmail;
        string destinaryStreet;
        string destinaryNumber;
        string destinaryBlock;
        string destinaryClient;
        //
        string remitentContactName;
        string remitentPhonenumber;
        string remitentEmail;
        string remitentStreet;
        string remitentNumber;
        string remitentBlock;
        string remitentClient;
        //
        string operatorName;
        string order;
        string packaging;
        string remitentCity;
        string remitentCompany;

        string remitentCountry;
        string remitentName;
        string remitentState;
        string remitentZipCode;
        string serviceName;
        string status;
        string tracking;
        string userName;
        string remitentNameR = "";
        string destinaryNameC = "";
        string secured = "";

        float value = 0;

        static string ChangeStatusLanguage(string status)
        {
            if (status != null)
            {
                byte[] tempBytes;
                tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(status.Replace("\t", ""));
                status = System.Text.Encoding.UTF8.GetString(tempBytes);
                tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(status.Replace("\n", ""));
                status = System.Text.Encoding.UTF8.GetString(tempBytes);
                tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(status.Replace("\r", ""));
                status = System.Text.Encoding.UTF8.GetString(tempBytes);
                switch (status)
                {
                    case "Shipment information sent to FedEx":
                        status = "Etiqueta creada";
                        break;
                    case "At FedEx origin facility":
                        status = "En las instalaciones de FedEx de origen";
                        break;
                    case "At destination sort facility":
                        status = "En las instalaciones de ordenado del destino";
                        break;
                    case "Picked up":
                        status = "Recolectado";
                        break;
                    case "Left FedEx origin facility":
                        status = "Dejo las instalaciones FedEx de origen";
                        break;
                    case "Arrived at FedEx location":
                        status = "Llego a destino FedEx";
                        break;
                    case "At local FedEx facility":
                        status = "En las instalaciones FedEx locales";
                        break;
                    case "Departed FedEx location":
                        status = "Salio de instalaciones FedEx de destino";
                        break;
                    case "On FedEx vehicle for delivery":
                        status = "En vehiculo de ruta para entrega";
                        break;
                    case "Delivered":
                        status = "Entregado";
                        break;
                    case "Shipment cancelled by sender":
                        status = "Envio cancelado por el cliente";
                        break;
                    case "Deleted tracking number":
                        status = "Numero de rastreo eliminado";
                        break;
                    case "Shipment exception":
                        status = "Excepcion de envio";
                        break;
                    case "Out for delivery":
                        status = "Fuera de entrega";
                        break;
                    case "In Transit":
                        status = "En transito";
                        break;
                    case "In transit":
                        status = "En transito";
                        break;
                    case "Clearance delay":
                        status = "Retenido en aduana";
                        break;
                    case "At FedEx destination facility":
                        status = "En las instalaciones FedEx de destino";
                        break;
                    case "Ready for pickup":
                        status = "Listo para su recoleccion";
                        break;
                    case "No description by fedex":
                        status = "Sin descripcion por FedEx";
                        break;
                    case "Delivery exception":
                        status = "Excepcion de entrega";
                        break;
                    case "Other Provider":
                        status = "Otro operador";
                        break;
                }
            }
            return status;
        }
        static int GetShipmentType(string company)
        {
            int shipmentType = 0;
            SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
            {
                DataSource = "Traps-carmar.Database.windows.net",
                UserID = "Thrimex",
                Password = "Caradmin2018!",
                InitialCatalog = "Invoicing"
            };
            using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
            {
                connection.Open();
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("SELECT TipoEnvio ");
                stringBuilder.Append("FROM Clientes_tbl ");
                
                if(company == "OSMAG")
                {
                    stringBuilder.Append("WHERE NombreEmpresa = '" + company + "' or NumeroControl = 'OSSA%'");
                    
                }
                else
                {
                    stringBuilder.Append("WHERE NombreEmpresa = '" + company + "'");
                }
                String stringResult = stringBuilder.ToString();
                using (SqlCommand command = new SqlCommand(stringResult, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            shipmentType = Convert.ToInt32(reader["TipoEnvio"]);

                        }
                    }
                }
                connection.Close();
            }
            return shipmentType;
        }
        void GetCustomApiData(string jsonObject)
        {
            try
            {
                var jsonResult = JObject.Parse(jsonObject);
                destinataryName = (string)jsonResult["Destinatario"]["NombreContacto"];
                order = (string)jsonResult["IdPedido"];
                tracking = (string)jsonResult["IdTracking"];
            }
            catch (Exception e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }

        }
        void GetData(string companyName, string jsonObject, string shipment)
        {
            try
            {
                shipmentType = GetShipmentType(companyName);
                var jsonResult = JObject.Parse(jsonObject);
                content = (string)jsonResult["envios"][0]["contenido"];
                destinataryCity = (string)jsonResult["destinatario"]["ciudad"];
                destinataryCompany = (string)jsonResult["destinatario"]["empresa"];
                destinataryCountry = (string)jsonResult["destinatario"]["pais"];
                destinataryName = (string)jsonResult["destinatario"]["nombreContacto"];
                destinataryState = (string)jsonResult["destinatario"]["estado"];
                destinataryZipCode = (string)jsonResult["destinatario"]["codigoPostal"];
                operatorName = (string)jsonResult["caracteristicas"]["operador"];
                packaging = (string)jsonResult["caracteristicas"]["tipoEnvio"];
                remitentCity = (string)jsonResult["remitente"]["ciudad"];
                remitentCountry = (string)jsonResult["remitente"]["pais"];
                remitentCompany = (string)jsonResult["remitente"]["empresa"];

                remitentName = (string)jsonResult["remitente"]["nombreContacto"];
                remitentState = (string)jsonResult["remitente"]["estado"];
                remitentZipCode = (string)jsonResult["remitente"]["codigoPostal"];
                serviceName = (string)jsonResult["caracteristicas"]["productoNombre"];
                userName = (string)jsonResult["NombreUsuario"];

                remitentNameR = (string)jsonResult["remitente"]["cliente"];
                destinaryNameC = (string)jsonResult["destinatario"]["cliente"];
                secured = (string)jsonResult["envio"]["asegurado"];


                if (((JArray)jsonResult["envios"]).Count > 1)
                {
                    for (int i = 0; i < ((JArray)jsonResult["envios"]).Count; i++)
                    {
                        var val = (JValue)jsonResult["envios"];
                        JValue auxHeight = (JValue)jsonResult["envios"][i]["alto"];
                        heightsList.Add(Convert.ToSingle(auxHeight));
                        JValue auxLength = (JValue)jsonResult["envios"][i]["largo"];
                        lengthsList.Add(Convert.ToSingle(auxHeight));
                        JValue auxSecuredValue = (JValue)jsonResult["envios"][i]["valor"];
                        securedValuesList.Add(Convert.ToSingle(auxHeight));
                        JValue auxWeight = (JValue)jsonResult["envios"][i]["peso"];
                        weightsList.Add(Convert.ToSingle(auxHeight));
                        JValue auxWidth = (JValue)jsonResult["envios"][i]["ancho"];
                        widthsList.Add(Convert.ToSingle(auxHeight));
                    }
                    if (shipmentType == 1)
                    {
                        for (int i = 0; i < ((JArray)jsonResult["envios"]).Count; i++)
                        {
                            height += Convert.ToSingle(Math.Ceiling(heightsList[i]));
                            length += Convert.ToSingle(Math.Ceiling(lengthsList[i]));
                            securedValue += Convert.ToSingle(Math.Ceiling(securedValuesList[i]));
                            weight += Convert.ToSingle(Math.Ceiling(weightsList[i]));
                            width += Convert.ToSingle(Math.Ceiling(widthsList[i]));
                        }
                    }
                }
                else
                {
                    height = (float)jsonResult["envio"][0]["alto"];
                    length = (float)jsonResult["envio"][0]["largo"];
                    securedValue = (float)jsonResult["envio"][0]["valor"];
                    weight = (float)jsonResult["envio"][0]["peso"];
                    width = (float)jsonResult["envio"][0]["ancho"];
                    //secured = (string)jsonResult["envio"][""]
                    shipmentType = 1;
                }
                try
                {
                    SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                    {
                        DataSource = "Traps-carmar.Database.windows.net",
                        UserID = "Thrimex",
                        Password = "Caradmin2018!",
                        InitialCatalog = "TRAPS2"
                    };
                    if (shipmentType == 1)
                    {
                        using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                        {
                            connection.Open();
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.Append("SELECT ECT_AWB ");
                            stringBuilder.Append("FROM MasterTable ");
                            stringBuilder.Append("56t AWB = '" + shipment + "'");
                            String stringResult = stringBuilder.ToString();
                            using (SqlCommand command = new SqlCommand(stringResult, connection))
                            {
                                using (SqlDataReader reader = command.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        tracking = Convert.ToString(reader["ECT_AWB"]);
                                    }
                                }
                            }
                            connection.Close();
                        }
                        using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                        {
                            connection.Open();
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.Append("SELECT EstadoFedex ");
                            stringBuilder.Append("FROM Estatus_Fedex ");
                            stringBuilder.Append("WHERE FedexTracking = '" + tracking + "'");
                            String stringResult = stringBuilder.ToString();
                            using (SqlCommand command = new SqlCommand(stringResult, connection))
                            {
                                using (SqlDataReader reader = command.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        status = Convert.ToString(reader["EstadoFedex"]);
                                    }
                                }
                            }
                            connection.Close();
                        }
                    }
                    else
                    {
                        using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                        {
                            connection.Open();
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.Append("SELECT * ");
                            stringBuilder.Append("FROM MasterTable ");
                            stringBuilder.Append("WHERE AWB LIKE '" + shipment + "%'");
                            String stringResult = stringBuilder.ToString();
                            using (SqlCommand command = new SqlCommand(stringResult, connection))
                            {
                                using (SqlDataReader reader = command.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        if (!trackingsList.Contains(reader[2].ToString()))
                                        {
                                            trackingsList.Add(reader[2].ToString());
                                        }
                                        
                                    }
                                }
                            }
                            connection.Close();
                        }
                        if (trackingsList.Count > 0)
                        {
                            using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                            {
                                connection.Open();
                                StringBuilder stringBuilder = new StringBuilder();
                                stringBuilder.Append("SELECT EstadoFedex ");
                                stringBuilder.Append("FROM Estatus_Fedex ");
                                stringBuilder.Append("WHERE FedexTracking = '" + trackingsList[0] + "'");
                                String stringResult = stringBuilder.ToString();
                                using (SqlCommand command = new SqlCommand(stringResult, connection))
                                {
                                    using (SqlDataReader reader = command.ExecuteReader())
                                    {
                                        while (reader.Read())
                                        {
                                            status = Convert.ToString(reader["EstadoFedex"]);
                                        }
                                    }
                                }
                                connection.Close();
                            }
                        }
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine("Hubo un error al generar el reporte," +
                        "\na continuacion se especifican los detalles del mismo:" +
                        "\n\n" + e.ToString() +
                        "\n\nFavor de contactar al administrador del sistema," +
                        "\npara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        void GetApiData(string companyName, string jsonObject, string shipment)
        {
            try
            {
                try
                {
                    shipmentType = GetShipmentType(companyName);
                }
                catch
                {

                }
                var jsonResult = JObject.Parse(jsonObject);
                content = (string)jsonResult["Paquete"][0]["Contenido"];
                destinataryCity = (string)jsonResult["Destinatario"]["Ciudad"];
                destinataryCompany = (string)jsonResult["Destinatario"]["Cliente"];
                destinataryCountry = (string)jsonResult["Destinatario"]["CodigoPais"];
                destinataryName = (string)jsonResult["Destinatario"]["NombreContacto"];
                destinataryState = (string)jsonResult["Destinatario"]["Estado"];
                destinataryZipCode = (string)jsonResult["Destinatario"]["CodigoPostal"];

                operatorName = (string)jsonResult["TipoCourier"];
                packaging = (string)jsonResult["TipoEnvio"];
                remitentCity = (string)jsonResult["Remitente"]["Ciudad"];
                remitentCountry = (string)jsonResult["Remitente"]["CodigoPais"];
                remitentCompany = (string)jsonResult["Remitente"]["Cliente"] + "";
                remitentName = (string)jsonResult["Remitente"]["NombreContacto"];
                remitentState = (string)jsonResult["Remitente"]["Estado"];
                remitentZipCode = (string)jsonResult["Remitente"]["CodigoPostal"];
                serviceName = (string)jsonResult["Envio"]["Producto"];
                userName = (string)jsonResult["NombreUsuarioCreacion"];

                //
                destinaryPhonenumber = (string)jsonResult["Destinatario"]["Telefono"];
                destinaryEmail = (string)jsonResult["Destinatario"]["Correo"];
                destinaryStreet = (string)jsonResult["Destinatario"]["Calle"];
                destinaryNumber = (string)jsonResult["Destinatario"]["Numero"];
                destinaryBlock = (string)jsonResult["Destinatario"]["Colonia"];
                destinaryClient = (string)jsonResult["Destinatario"]["Cliente"];
                //
                remitentPhonenumber = (string)jsonResult["Remitente"]["Telefono"];
                remitentEmail = (string)jsonResult["Remitente"]["Correo"];
                remitentStreet = (string)jsonResult["Remitente"]["Calle"];
                remitentNumber = (string)jsonResult["Remitente"]["Numero"];
                remitentBlock = (string)jsonResult["Remitente"]["Colonia"];
                remitentClient = (string)jsonResult["Remitente"]["Cliente"];
                string b64 = (string)jsonResult["FileB64"];
                tracking = (string)jsonResult["IdTracking"];
                var generadas = (JArray)jsonResult["GuiasGeneradas"];

                if (((JArray)jsonResult["Paquete"]).Count > 1)
                {
                    for (int i = 0; i < ((JArray)jsonResult["Paquete"]).Count; i++)
                    {
                        JValue auxHeight = (JValue)jsonResult["Paquete"][i]["Alto"];
                        heightsList.Add(Convert.ToSingle(auxHeight));
                        JValue auxLength = (JValue)jsonResult["Paquete"][i]["Largo"];
                        lengthsList.Add(Convert.ToSingle(auxHeight));
                        JValue auxSecuredValue = (JValue)jsonResult["Paquete"][i]["Valor"];
                        securedValuesList.Add(Convert.ToSingle(auxHeight));
                        JValue auxWeight = (JValue)jsonResult["Paquete"][i]["Peso"];
                        weightsList.Add(Convert.ToSingle(auxHeight));
                        JValue auxWidth = (JValue)jsonResult["Paquete"][i]["Ancho"];
                        widthsList.Add(Convert.ToSingle(auxHeight));
                        try
                        {
                            var auxTrack = generadas[i];
                            string trackElement = Convert.ToString(auxTrack);
                            if (!trackingsList.Contains(trackElement))
                                {
                                    trackingsList.Add(trackElement);
                                }
                        
                        }
                        catch { }
                    }
                    if (shipmentType == 1)
                    {
                        for (int i = 0; i < ((JArray)jsonResult["Paquete"]).Count; i++)
                        {
                            height += Convert.ToSingle(Math.Ceiling(heightsList[i]));
                            length += Convert.ToSingle(Math.Ceiling(lengthsList[i]));
                            securedValue += Convert.ToSingle(Math.Ceiling(securedValuesList[i]));
                            weight += Convert.ToSingle(Math.Ceiling(weightsList[i]));
                            width += Convert.ToSingle(Math.Ceiling(widthsList[i]));
                        }
                    }
                }
                else
                {
                    height = (float)jsonResult["Paquete"][0]["Alto"];
                    length = (float)jsonResult["Paquete"][0]["Largo"];
                    securedValue = (float)jsonResult["Paquete"][0]["Valor"];
                    weight = (float)jsonResult["Paquete"][0]["Peso"];
                    width = (float)jsonResult["Paquete"][0]["Ancho"];
                    shipmentType = 1;
                }
                try
                {
                    SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                    {
                        DataSource = "Traps-carmar.Database.windows.net",
                        UserID = "Thrimex",
                        Password = "Caradmin2018!",
                        InitialCatalog = "TRAPS2"
                    };
                    if (shipmentType == 1)
                    {
                        using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                        {
                            connection.Open();
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.Append("SELECT ECT_AWB ");
                            stringBuilder.Append("FROM MasterTable ");
                            stringBuilder.Append("WHERE AWB = '" + shipment + "'");
                            String stringResult = stringBuilder.ToString();
                            using (SqlCommand command = new SqlCommand(stringResult, connection))
                            {
                                using (SqlDataReader reader = command.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        tracking = Convert.ToString(reader["ECT_AWB"]);
                                    }
                                }
                            }
                            connection.Close();
                        }

                        SqlConnectionStringBuilder stringBuilderSql2 = new SqlConnectionStringBuilder()
                        {
                            DataSource = "Traps-carmar.Database.windows.net",
                            UserID = "Thrimex",
                            Password = "Caradmin2018!",
                            InitialCatalog = "TRAPS-ADMIN"
                        };
                        using (SqlConnection connection = new SqlConnection(stringBuilderSql2.ConnectionString))
                        {
                            connection.Open();
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.Append("SELECT * ");
                            stringBuilder.Append("FROM Operadores ");
                            stringBuilder.Append("WHERE Id = " + operatorName);
                            String stringResult = stringBuilder.ToString();
                            using (SqlCommand command = new SqlCommand(stringResult, connection))
                            {
                                using (SqlDataReader reader = command.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        operatorName = reader[1].ToString();
                                    }
                                }
                            }
                            connection.Close();
                        }
                        using (SqlConnection connection = new SqlConnection(stringBuilderSql2.ConnectionString))
                        {
                            connection.Open();
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.Append("SELECT * ");
                            stringBuilder.Append("FROM Servicios ");
                            stringBuilder.Append("WHERE Id = " + serviceName);
                            String stringResult = stringBuilder.ToString();
                            using (SqlCommand command = new SqlCommand(stringResult, connection))
                            {
                                using (SqlDataReader reader = command.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        serviceName = reader[1].ToString();
                                    }
                                }
                            }
                            connection.Close();
                        }

                        using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                        {
                            connection.Open();
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.Append("SELECT EstadoFedex ");
                            stringBuilder.Append("FROM Estatus_Fedex ");
                            stringBuilder.Append("WHERE FedexTracking = '" + tracking + "'");
                            String stringResult = stringBuilder.ToString();
                            using (SqlCommand command = new SqlCommand(stringResult, connection))
                            {
                                using (SqlDataReader reader = command.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        status = Convert.ToString(reader["EstadoFedex"]);
                                    }
                                }
                            }
                            connection.Close();
                        }
                    }
                    else
                    {
                        using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                        {
                            connection.Open();
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.Append("SELECT * ");
                            stringBuilder.Append("FROM MasterTable ");
                            stringBuilder.Append("WHERE AWB LIKE '" + shipment + "%'");
                            String stringResult = stringBuilder.ToString();
                            using (SqlCommand command = new SqlCommand(stringResult, connection))
                            {
                                using (SqlDataReader reader = command.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        string trackElement = reader[2].ToString();
                                        if (!trackingsList.Contains(trackElement))
                                        {
                                            trackingsList.Add(trackElement);
                                        }
                                        //trackingsList.Add(reader[2].ToString());
                                    }
                                }
                            }
                            connection.Close();
                        }
                        SqlConnectionStringBuilder stringBuilderSql2 = new SqlConnectionStringBuilder()
                        {
                            DataSource = "Traps-carmar.Database.windows.net",
                            UserID = "Thrimex",
                            Password = "Caradmin2018!",
                            InitialCatalog = "TRAPS-ADMIN"
                        };
                        using (SqlConnection connection = new SqlConnection(stringBuilderSql2.ConnectionString))
                        {
                            connection.Open();
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.Append("SELECT * ");
                            stringBuilder.Append("FROM Operadores ");
                            stringBuilder.Append("WHERE Id = " + operatorName);
                            String stringResult = stringBuilder.ToString();
                            using (SqlCommand command = new SqlCommand(stringResult, connection))
                            {
                                using (SqlDataReader reader = command.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        operatorName = reader[1].ToString();
                                    }
                                }
                            }
                            connection.Close();
                        }
                        using (SqlConnection connection = new SqlConnection(stringBuilderSql2.ConnectionString))
                        {
                            connection.Open();
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.Append("SELECT * ");
                            stringBuilder.Append("FROM Servicios ");
                            stringBuilder.Append("WHERE Id = " + serviceName);
                            String stringResult = stringBuilder.ToString();
                            using (SqlCommand command = new SqlCommand(stringResult, connection))
                            {
                                using (SqlDataReader reader = command.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        serviceName = reader[1].ToString();
                                    }
                                }
                            }
                            connection.Close();
                        }
                        if (trackingsList.Count > 0)
                        {
                            using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                            {
                                connection.Open();
                                StringBuilder stringBuilder = new StringBuilder();
                                stringBuilder.Append("SELECT EstadoFedex ");
                                stringBuilder.Append("FROM Estatus_Fedex ");
                                stringBuilder.Append("WHERE FedexTracking = '" + trackingsList[0] + "'");
                                String stringResult = stringBuilder.ToString();
                                using (SqlCommand command = new SqlCommand(stringResult, connection))
                                {
                                    using (SqlDataReader reader = command.ExecuteReader())
                                    {
                                        while (reader.Read())
                                        {
                                            status = Convert.ToString(reader["EstadoFedex"]);
                                        }
                                    }
                                }
                                connection.Close();
                            }
                        }
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine("Hubo un error al generar el reporte," +
                        "\na continuacion se especifican los detalles del mismo:" +
                        "\n\n" + e.ToString() +
                        "\n\n" + jsonObject +
                        "\n\nFavor de contactar al administrador del sistema," +
                        "\npara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        void GetLikkusData(string jsonObject)
        {
            try
            {
                var jsonResult = JObject.Parse(jsonObject);
                order = (string)jsonResult["destinatario"]["empresa"];
                shipmentType = 1;
            }
            catch (Exception e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }

        }
        void GetRAGAData(string jsonObject, string shipment)
        {
            try
            {
                var jsonResult = JObject.Parse(jsonObject);
                destinataryName = (string)jsonResult["destinatario"]["nombreContacto"];
                order = "N/A";
                try
                {
                    SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                    {
                        DataSource = "Traps-carmar.Database.windows.net",
                        UserID = "Thrimex",
                        Password = "Caradmin2018!",
                        InitialCatalog = "TRAPS2"
                    };
                    using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                    {
                        connection.Open();
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append("SELECT ECT_AWB ");
                        stringBuilder.Append("FROM MasterTable ");
                        stringBuilder.Append("WHERE AWB = '" + shipment + "'");
                        String stringResult = stringBuilder.ToString();
                        using (SqlCommand command = new SqlCommand(stringResult, connection))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    tracking = Convert.ToString(reader["ECT_AWB"]);
                                }
                            }
                        }
                        connection.Close();
                    }
                    using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                    {
                        connection.Open();
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append("SELECT EstadoFedex ");
                        stringBuilder.Append("FROM Estatus_Fedex ");
                        stringBuilder.Append("WHERE FedexTracking = '" + tracking + "'");
                        String stringResult = stringBuilder.ToString();
                        using (SqlCommand command = new SqlCommand(stringResult, connection))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    status = Convert.ToString(reader["EstadoFedex"]);
                                }
                            }
                        }
                        connection.Close();
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine("Hubo un error al generar el reporte," +
                        "\na continuacion se especifican los detalles del mismo:" +
                        "\n\n" + e.ToString() +
                        "\n\nFavor de contactar al administrador del sistema," +
                        "\npara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }

        }
        int tipoCourier = 0;
        void GetEstafetaData(string jsonObject, string shipment)
        {
            try
            {
                var jsonResult = JObject.Parse(jsonObject);
                destinataryName = (string)jsonResult["destinatario"]["nombreContacto"];
                tipoCourier = (int)jsonResult["TipoCourier"];
                order = "N/A";
                try
                {
                    //SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                    //{
                    //    DataSource = "Traps-carmar.Database.windows.net",
                    //    UserID = "Thrimex",
                    //    Password = "Caradmin2018!",
                    //    InitialCatalog = "TRAPS2"
                    //};
                    //using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                    //{
                    //    connection.Open();
                    //    StringBuilder stringBuilder = new StringBuilder();
                    //    stringBuilder.Append("SELECT ECT_AWB ");
                    //    stringBuilder.Append("FROM MasterTable ");
                    //    stringBuilder.Append("WHERE AWB = '" + shipment + "'");
                    //    String stringResult = stringBuilder.ToString();
                    //    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    //    {
                    //        using (SqlDataReader reader = command.ExecuteReader())
                    //        {
                    //            while (reader.Read())
                    //            {
                    //                tracking = Convert.ToString(reader["ECT_AWB"]);
                    //            }
                    //        }
                    //    }
                    //    connection.Close();
                    //}
                    //using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                    //{
                    //    connection.Open();
                    //    StringBuilder stringBuilder = new StringBuilder();
                    //    stringBuilder.Append("SELECT EstadoFedex ");
                    //    stringBuilder.Append("FROM Estatus_Fedex ");
                    //    stringBuilder.Append("WHERE FedexTracking = '" + tracking + "'");
                    //    String stringResult = stringBuilder.ToString();
                    //    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    //    {
                    //        using (SqlDataReader reader = command.ExecuteReader())
                    //        {
                    //            while (reader.Read())
                    //            {
                    //                status = Convert.ToString(reader["EstadoFedex"]);
                    //            }
                    //        }
                    //    }
                    //    connection.Close();
                    //}
                }
                catch (SqlException e)
                {
                    Console.WriteLine("Hubo un error al generar el reporte," +
                        "\na continuacion se especifican los detalles del mismo:" +
                        "\n\n" + e.ToString() +
                        "\n\nFavor de contactar al administrador del sistema," +
                        "\npara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }

        }
        void GetRAGAApiData(string jsonObject)
        {
            try
            {
                var jsonResult = JObject.Parse(jsonObject);
                destinataryName = (string)jsonResult["Destinatario"]["NombreContacto"];
                order = (string)jsonResult["IdPedido"];
                tracking = (string)jsonResult["IdTracking"];
                try
                {
                    SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                    {
                        DataSource = "Traps-carmar.Database.windows.net",
                        UserID = "Thrimex",
                        Password = "Caradmin2018!",
                        InitialCatalog = "TRAPS2"
                    };
                    using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                    {
                        connection.Open();
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append("SELECT EstadoFedex ");
                        stringBuilder.Append("FROM Estatus_Fedex ");
                        stringBuilder.Append("WHERE FedexTracking = '" + tracking + "'");
                        String stringResult = stringBuilder.ToString();
                        using (SqlCommand command = new SqlCommand(stringResult, connection))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    status = Convert.ToString(reader["EstadoFedex"]);
                                }
                            }
                        }
                        connection.Close();
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine("Hubo un error al generar el reporte," +
                        "\na continuacion se especifican los detalles del mismo:" +
                        "\n\n" + e.ToString() +
                        "\n\nFavor de contactar al administrador del sistema," +
                        "\npara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }

        }

        void GetEstafetaData(string jsonObject)
        {
            try
            {
                var jsonResult = JObject.Parse(jsonObject);
                destinataryName = (string)jsonResult["Destinatario"]["NombreContacto"];
                order = (string)jsonResult["IdPedido"];
                tracking = (string)jsonResult["IdTracking"];
                //try
                //{
                //    SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                //    {
                //        DataSource = "Traps-carmar.Database.windows.net",
                //        UserID = "Thrimex",
                //        Password = "Caradmin2018!",
                //        InitialCatalog = "TRAPS2"
                //    };
                //    using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                //    {
                //        connection.Open();
                //        StringBuilder stringBuilder = new StringBuilder();
                //        stringBuilder.Append("SELECT EstadoFedex ");
                //        stringBuilder.Append("FROM Estatus_Fedex ");
                //        stringBuilder.Append("WHERE FedexTracking = '" + tracking + "'");
                //        String stringResult = stringBuilder.ToString();
                //        using (SqlCommand command = new SqlCommand(stringResult, connection))
                //        {
                //            using (SqlDataReader reader = command.ExecuteReader())
                //            {
                //                while (reader.Read())
                //                {
                //                    status = Convert.ToString(reader["EstadoFedex"]);
                //                }
                //            }
                //        }
                //        connection.Close();
                //    }
                //}
                //catch (SqlException e)
                //{
                //    Console.WriteLine("Hubo un error al generar el reporte," +
                //        "\na continuacion se especifican los detalles del mismo:" +
                //        "\n\n" + e.ToString() +
                //        "\n\nFavor de contactar al administrador del sistema," +
                //        "\npara volver al menu principal favor de presionar la tecla Y...");
                //    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                //    {

                //    }
                //}
            
            }
            catch (Exception e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }

        }


        void GetSamsoniteData(string jsonObject, string shipment)
        {
            try
            {
                var jsonResult = JObject.Parse(jsonObject);
                remitentName = (string)jsonResult["Remitente"]["NombreContacto"];
                remitentNameR = (string)jsonResult["Remitente"]["Cliente"];
                destinaryNameC = (string)jsonResult["Destinatario"]["Cliente"];
                destinataryCity = (string)jsonResult["Destinatario"]["Ciudad"];
                destinataryName = (string)jsonResult["Destinatario"]["NombreContacto"];
                destinataryState = (string)jsonResult["Destinatario"]["Estado"];
                destinataryZipCode = (string)jsonResult["Destinatario"]["CodigoPostal"];
                secured = (string)jsonResult["Envio"]["Asegurado"];
                var generadas = (JArray)jsonResult["GuiasGeneradas"];
                if (secured == "False") { secured = "No"; }
                else if (secured == "True") { secured = "Si"; }


                tracking = (string)jsonResult["IdTracking"];
                
                if (((JArray)jsonResult["Paquete"]).Count > 1)
                {
                    for (int i = 0; i < ((JArray)jsonResult["Paquete"]).Count; i++)
                    {

                        JValue auxHeight = (JValue)jsonResult["Paquete"][i]["Alto"];
                        heightsList.Add(Convert.ToSingle(auxHeight));
                        JValue auxLength = (JValue)jsonResult["Paquete"][i]["Largo"];
                        lengthsList.Add(Convert.ToSingle(auxHeight));

                        JValue auxSecuredValue = (JValue)jsonResult["Paquete"][i]["Valor"];
                        securedValuesList.Add(Convert.ToSingle(auxHeight));

                        JValue auxWeight = (JValue)jsonResult["Paquete"][i]["Peso"];
                        weightsList.Add(Convert.ToSingle(auxHeight));

                        JValue auxWidth = (JValue)jsonResult["Paquete"][i]["Ancho"];
                        widthsList.Add(Convert.ToSingle(auxHeight));
                        try {
                            var auxTrack = generadas[i];
                            string trackElement = Convert.ToString(auxTrack);
                            if (!trackingsList.Contains(trackElement))
                            {
                                trackingsList.Add(trackElement);
                            }
                            //trackingsList.Add(Convert.ToString(auxTrack));
                        } catch { }
                        
                    }
                    if (shipmentType == 1)
                    {
                        for (int i = 0; i < ((JArray)jsonResult["Paquete"]).Count; i++)
                        {
                            height += Convert.ToSingle(Math.Ceiling(heightsList[i]));
                            length += Convert.ToSingle(Math.Ceiling(lengthsList[i]));
                            securedValue += Convert.ToSingle(Math.Ceiling(securedValuesList[i]));
                            weight += Convert.ToSingle(Math.Ceiling(weightsList[i]));
                            width += Convert.ToSingle(Math.Ceiling(widthsList[i]));
                            value += Convert.ToSingle(Math.Ceiling(securedValuesList[i]));
                        }
                    }
                }
                else
                {
                    height = (float)jsonResult["Paquete"][0]["Alto"];
                    length = (float)jsonResult["Paquete"][0]["Largo"];
                    securedValue = (float)jsonResult["Paquete"][0]["Valor"];
                    weight = (float)jsonResult["Paquete"][0]["Peso"];
                    width = (float)jsonResult["Paquete"][0]["Ancho"];
                    secured = (string)jsonResult["Envio"]["Asegurado"];
                    if (secured == "False") { secured = "No"; }
                    else if (secured == "True") { secured = "Si"; }
                    value = (float)jsonResult["Paquete"][0]["Valor"];
                    //shipmentType = 1;
                }

                try
                {
                    SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                    {
                        DataSource = "Traps-carmar.Database.windows.net",
                        UserID = "Thrimex",
                        Password = "Caradmin2018!",
                        InitialCatalog = "TRAPS2"
                    };
                    using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                    {
                        connection.Open();
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append("SELECT ECT_AWB ");
                        stringBuilder.Append("FROM MasterTable ");
                        stringBuilder.Append("WHERE AWB = '" + shipment + "'");
                        String stringResult = stringBuilder.ToString();
                        using (SqlCommand command = new SqlCommand(stringResult, connection))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    tracking = Convert.ToString(reader["ECT_AWB"]);
                                }
                            }
                        }
                        connection.Close();
                    }
                    using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                    {
                        connection.Open();
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append("SELECT EstadoFedex ");
                        stringBuilder.Append("FROM Estatus_Fedex ");
                        stringBuilder.Append("WHERE FedexTracking = '" + tracking + "'");
                        String stringResult = stringBuilder.ToString();
                        using (SqlCommand command = new SqlCommand(stringResult, connection))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    status = Convert.ToString(reader["EstadoFedex"]);
                                }
                            }
                        }
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine("Hubo un error al generar el reporte," +
                        "\na continuacion se especifican los detalles del mismo:" +
                        "\n\n" + e.ToString() +
                        "\n\nFavor de contactar al administrador del sistema," +
                        "\npara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Hubo un error al generar el reporte, " +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        void GetSamsoniteApiData(string jsonObject)
        {
            try
            {
                var jsonResult = JObject.Parse(jsonObject);
                remitentName = (string)jsonResult["Remitente"]["NombreContacto"];
                destinataryCity = (string)jsonResult["Destinatario"]["Ciudad"];
                destinataryName = (string)jsonResult["Destinatario"]["NombreContacto"];
                destinataryState = (string)jsonResult["Destinatario"]["Estado"];
                destinataryZipCode = (string)jsonResult["Destinatario"]["CodigoPostal"];
                destinataryState = (string)jsonResult["Destinatario"]["Estado"];
                destinataryZipCode = (string)jsonResult["Destinatario"]["CodigoPostal"];
                remitentNameR = (string)jsonResult["Remitente"]["Cliente"];
                destinaryNameC = (string)jsonResult["Destinatario"]["Cliente"];
                secured = (string)jsonResult["Envio"]["Asegurado"];
                var generadas = (JArray)jsonResult["GuiasGeneradas"];
                if (secured == "False") { secured = "No"; }
                else if (secured == "True") { secured = "Si"; }


                tracking = (string)jsonResult["IdTracking"];

                if (((JArray)jsonResult["Paquete"]).Count > 1)
                {
                    for (int i = 0; i < ((JArray)jsonResult["Paquete"]).Count; i++)
                    {

                        JValue auxHeight = (JValue)jsonResult["Paquete"][i]["Alto"];
                        heightsList.Add(Convert.ToSingle(auxHeight));

                        JValue auxLength = (JValue)jsonResult["Paquete"][i]["Largo"];
                        lengthsList.Add(Convert.ToSingle(auxHeight));

                        JValue auxSecuredValue = (JValue)jsonResult["Paquete"][i]["Valor"];
                        securedValuesList.Add(Convert.ToSingle(auxHeight));

                        JValue auxWeight = (JValue)jsonResult["Paquete"][i]["Peso"];
                        weightsList.Add(Convert.ToSingle(auxHeight));
                        JValue auxWidth = (JValue)jsonResult["Paquete"][i]["Ancho"];
                        widthsList.Add(Convert.ToSingle(auxHeight));
                        try
                        {
                            var auxTrack = generadas[i];
                            if (!trackingsList.Contains(Convert.ToString(auxTrack)))
                            {
                                trackingsList.Add(Convert.ToString(auxTrack));
                            }
                            


                        }
                        catch { }

                    }
                    if (shipmentType == 1)
                    {
                        for (int i = 0; i < ((JArray)jsonResult["Paquete"]).Count; i++)
                        {
                            height += Convert.ToSingle(Math.Ceiling(heightsList[i]));
                            length += Convert.ToSingle(Math.Ceiling(lengthsList[i]));
                            securedValue += Convert.ToSingle(Math.Ceiling(securedValuesList[i]));
                            weight += Convert.ToSingle(Math.Ceiling(weightsList[i]));
                            width += Convert.ToSingle(Math.Ceiling(widthsList[i]));
                            value += Convert.ToSingle(Math.Ceiling(securedValuesList[i]));
                        }
                    }
                }
                else
                {
                    height = (float)jsonResult["Paquete"][0]["Alto"];
                    length = (float)jsonResult["Paquete"][0]["Largo"];
                    securedValue = (float)jsonResult["Paquete"][0]["Valor"];
                    weight = (float)jsonResult["Paquete"][0]["Peso"];
                    width = (float)jsonResult["Paquete"][0]["Ancho"];
                    secured = (string)jsonResult["Envio"]["Asegurado"];
                    if (secured == "False") { secured = "No"; }
                    else if (secured == "True") { secured = "Si"; }
                    value = (float)jsonResult["Paquete"][0]["Valor"];
                    //shipmentType = 1;
                }

                try
                {
                    SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                    {
                        DataSource = "Traps-carmar.Database.windows.net",
                        UserID = "Thrimex",
                        Password = "Caradmin2018!",
                        InitialCatalog = "TRAPS2"
                    };
                    using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                    {
                        connection.Open();
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append("SELECT EstadoFedex ");
                        stringBuilder.Append("FROM Estatus_Fedex ");
                        stringBuilder.Append("WHERE FedexTracking = '" + tracking + "'");
                        String stringResult = stringBuilder.ToString();
                        using (SqlCommand command = new SqlCommand(stringResult, connection))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    status = Convert.ToString(reader["EstadoFedex"]);
                                }
                            }
                        }
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine("Hubo un error al generar el reporte," +
                        "\na continuacion se especifican los detalles del mismo:" +
                        "\n\n" + e.ToString() +
                        "\n\nFavor de contactar al administrador del sistema," +
                        "\npara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Hubo un error al generar el reporte, " +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        void GetSamsoniteRetornosData(string jsonObject, string shipment)
        {
            try
            {
                var jsonResult = JObject.Parse(jsonObject);
                remitentName = (string)jsonResult["Remitente"]["NombreContacto"];
                try
                {
                    SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                    {
                        DataSource = "Traps-carmar.Database.windows.net",
                        UserID = "Thrimex",
                        Password = "Caradmin2018!",
                        InitialCatalog = "TRAPS2"
                    };
                    using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                    {
                        connection.Open();
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append("SELECT ECT_AWB ");
                        stringBuilder.Append("FROM MasterTable ");
                        stringBuilder.Append("WHERE AWB LIKE '" + shipment + "%'");
                        String stringResult = stringBuilder.ToString();
                        using (SqlCommand command = new SqlCommand(stringResult, connection))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    if (!trackingsList.Contains(reader[0].ToString())){
                                        trackingsList.Add(reader[0].ToString());
                                    }
                                    
                                }
                            }
                        }
                        connection.Close();
                    }
                    foreach (var track in trackingsList)
                    {
                        using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                        {
                            connection.Open();
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.Append("SELECT EstadoFedex ");
                            stringBuilder.Append("FROM Estatus_Fedex ");
                            stringBuilder.Append("WHERE FedexTracking = '" + track + "'");
                            String stringResult = stringBuilder.ToString();
                            using (SqlCommand command = new SqlCommand(stringResult, connection))
                            {
                                using (SqlDataReader reader = command.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        statusList.Add(reader[0].ToString());
                                    }
                                }
                            }
                        }
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine("Hubo un error al generar el reporte," +
                        "\na continuacion se especifican los detalles del mismo:" +
                        "\n\n" + e.ToString() +
                        "\n\nFavor de contactar al administrador del sistema," +
                        "\npara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Hubo un error al generar el reporte, " +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        void GetSotanoData(string jsonObject, string shipment)
        {
            try
            {
                var jsonResult = JObject.Parse(jsonObject);
                destinataryCity = (string)jsonResult["destinatario"]["ciudad"];
                destinataryState = (string)jsonResult["destinatario"]["estado"];
                destinataryZipCode = (string)jsonResult["destinatario"]["codigoPostal"];
                remitentCity = (string)jsonResult["remitente"]["ciudad"];
                remitentState = (string)jsonResult["remitente"]["estado"];
                remitentZipCode = (string)jsonResult["remitente"]["codigoPostal"];
                serviceName = (string)jsonResult["caracteristicas"]["productoNombre"];
                try
                {
                    SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                    {
                        DataSource = "Traps-carmar.Database.windows.net",
                        UserID = "Thrimex",
                        Password = "Caradmin2018!",
                        InitialCatalog = "TRAPS2"
                    };
                    using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                    {
                        connection.Open();
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append("SELECT ECT_AWB ");
                        stringBuilder.Append("FROM MasterTable ");
                        stringBuilder.Append("WHERE AWB = '" + shipment + "'");
                        String stringResult = stringBuilder.ToString();
                        using (SqlCommand command = new SqlCommand(stringResult, connection))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    tracking = Convert.ToString(reader["ECT_AWB"]);
                                }
                            }
                        }
                        connection.Close();
                    }
                    using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                    {
                        connection.Open();
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append("SELECT EstadoFedex ");
                        stringBuilder.Append("FROM Estatus_Fedex ");
                        stringBuilder.Append("WHERE FedexTracking = '" + tracking + "'");
                        String stringResult = stringBuilder.ToString();
                        using (SqlCommand command = new SqlCommand(stringResult, connection))
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    status = Convert.ToString(reader["EstadoFedex"]);
                                }
                            }
                        }
                        connection.Close();
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine("Hubo un error al generar el reporte," +
                        "\na continuacion se especifican los detalles del mismo:" +
                        "\n\n" + e.ToString() +
                        "\n\nFavor de contactar al administrador del sistema," +
                        "\npara volver al menu principal favor de presionar la tecla Y...");
                    while (Console.ReadKey(true).Key != ConsoleKey.Y)
                    {

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Hubo un error al generar el reporte, " +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        void GetVCVApiData(string jsonObject)
        {
            try
            {
                var jsonResult = JObject.Parse(jsonObject);
                destinataryCity = (string)jsonResult["Destinatario"]["Ciudad"];
                destinataryCompany = (string)jsonResult["Destinatario"]["Cliente"];
                destinataryCountry = (string)jsonResult["Destinatario"]["CodigoPais"];
                destinataryName = (string)jsonResult["Destinatario"]["NombreContacto"];
                destinataryState = (string)jsonResult["Destinatario"]["Estado"];
                destinataryZipCode = (string)jsonResult["Destinatario"]["CodigoPostal"];
                remitentName = (string)jsonResult["Remitente"]["NombreContacto"];
                operatorName = (string)jsonResult["TipoCourier"];
                tracking = (string)jsonResult["IdTracking"];

                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS2"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM Operadores ");
                    stringBuilder.Append("WHERE Id = " + operatorName);
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                operatorName = reader[1].ToString();
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }

        }
        public void CreateShipmentsReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> CompanyNamesList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl NOT LIKE 'VCV%' ");
                    stringBuilder.Append("AND NumeroControl NOT LIKE 'ELEM%' ");
                    stringBuilder.Append("AND NumeroControl NOT LIKE 'RAGA%' ");
                    stringBuilder.Append("AND NumeroControl NOT LIKE 'SAME%' ");
                    stringBuilder.Append("AND NumeroControl NOT LIKE 'SARE%' ");
                    stringBuilder.Append("AND NumeroControl NOT LIKE 'LISO%' ");
                    stringBuilder.Append("AND NumeroControl NOT LIKE 'BACL%' ");
                    stringBuilder.Append("AND NumeroControl NOT LIKE 'GASM%' ");
                    stringBuilder.Append("AND NumeroControl NOT LIKE 'JEAL%' ");
                    stringBuilder.Append("AND NumeroControl NOT LIKE 'BASU%' ");
                    stringBuilder.Append("AND NumeroControl NOT LIKE 'LIKK%' ");
                    stringBuilder.Append("AND NumeroControl NOT LIKE 'UFFI%' ");
                    stringBuilder.Append("AND NumeroControl NOT LIKE 'BERD%' ");
                    stringBuilder.Append("AND FechaCreacion > '2021-04-13'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesList.Add(reader[2].ToString());
                                CompanyNamesList.Add(reader[1].ToString());
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte general de envios";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                int longitudLista = ShipmentsList.Count;
                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    if (ShipmentsList[i] != "EIRG2806201911854"
                        && ShipmentsList[i] != "EIRG1707201912045"
                        && ShipmentsList[i] != "TLMX2308201910716"
                        && ShipmentsList[i] != "TLMX3107201910698"
                        && ShipmentsList[i] != "TLMX0209201910722"
                        && ShipmentsList[i] != "CCOL2609201910268"
                        && ShipmentsList[i] != "EIRG0810201912604"
                        && ShipmentsList[i] != "CCOL2210201910286"
                        && ShipmentsList[i] != "AURU0111201911058")
                    {
                        GetApiData(CompanyNamesList[i], JsonObjectsList[i], ShipmentsList[i]);
                        status = ChangeStatusLanguage(status);
                        if (status != "Entregado"
                            && status != "Sin descripcion por FedEx"
                            && status != "Envio cancelado por el cliente"
                            && status != "Completo por Seguimiento"
                            && Convert.ToDateTime(CreationDatesList[i]).Year != 2018)
                        {
                            if (shipmentType == 0)
                            {
                                if (trackingsList.Count > 0)
                                {
                                    for (int j = 0; j < trackingsList.Count; j++)
                                    {
                                        rowCount++;
                                        xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                                        xlWorksheet.Cells[rowCount, 1] = trackingsList[j];
                                        if (j == 0)
                                            xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i];
                                        else
                                            xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i] + "-" + j;
                                        xlWorksheet.Cells[rowCount, 3] = securedValuesList[j];
                                        xlWorksheet.Cells[rowCount, 4] = operatorName;
                                        xlWorksheet.Cells[rowCount, 5] = serviceName;
                                        xlWorksheet.Cells[rowCount, 6] = remitentName;
                                        xlWorksheet.Cells[rowCount, 7] = remitentCompany;
                                        xlWorksheet.Cells[rowCount, 8] = remitentCity;
                                        xlWorksheet.Cells[rowCount, 9] = remitentZipCode;
                                        xlWorksheet.Cells[rowCount, 10] = remitentState;
                                        xlWorksheet.Cells[rowCount, 11] = destinataryName;
                                        xlWorksheet.Cells[rowCount, 12] = destinataryCity;
                                        xlWorksheet.Cells[rowCount, 13] = destinataryZipCode;
                                        xlWorksheet.Cells[rowCount, 14] = destinataryState;
                                        xlWorksheet.Cells[rowCount, 15] = heightsList[j];
                                        xlWorksheet.Cells[rowCount, 16] = widthsList[j];
                                        xlWorksheet.Cells[rowCount, 17] = lengthsList[j];
                                        xlWorksheet.Cells[rowCount, 18] = content;
                                        xlWorksheet.Cells[rowCount, 19] = weightsList[j];
                                        xlWorksheet.Cells[rowCount, 20] = status;
                                        xlWorksheet.Cells[rowCount, 21] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");

                                        Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                                    }
                                }
                            }
                            else
                            {
                                if (tracking != "")
                                {
                                    rowCount++;
                                    xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                                    xlWorksheet.Cells[rowCount, 1] = tracking;
                                    xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i];
                                    xlWorksheet.Cells[rowCount, 3] = securedValue;
                                    xlWorksheet.Cells[rowCount, 4] = operatorName;
                                    xlWorksheet.Cells[rowCount, 5] = serviceName;
                                    xlWorksheet.Cells[rowCount, 6] = remitentName;
                                    xlWorksheet.Cells[rowCount, 7] = remitentCompany;
                                    xlWorksheet.Cells[rowCount, 8] = remitentCity;
                                    xlWorksheet.Cells[rowCount, 9] = remitentZipCode;
                                    xlWorksheet.Cells[rowCount, 10] = remitentState;
                                    xlWorksheet.Cells[rowCount, 11] = destinataryName;
                                    xlWorksheet.Cells[rowCount, 12] = destinataryCity;
                                    xlWorksheet.Cells[rowCount, 13] = destinataryZipCode;
                                    xlWorksheet.Cells[rowCount, 14] = destinataryState;
                                    xlWorksheet.Cells[rowCount, 15] = height;
                                    xlWorksheet.Cells[rowCount, 16] = width;
                                    xlWorksheet.Cells[rowCount, 17] = length;
                                    xlWorksheet.Cells[rowCount, 18] = content;
                                    xlWorksheet.Cells[rowCount, 19] = weight;
                                    xlWorksheet.Cells[rowCount, 20] = status;
                                    xlWorksheet.Cells[rowCount, 21] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");

                                    Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                                }
                            }
                        }
                    }

                    content = "";
                    destinataryCity = "";
                    destinataryName = "";
                    destinataryState = "";
                    destinataryZipCode = "";
                    destinataryZipCode = "";
                    height = 0;
                    heightsList.Clear();
                    length = 0;
                    lengthsList.Clear();
                    operatorName = "";
                    remitentCity = "";
                    remitentName = "";
                    remitentCompany = "";
                    remitentState = "";
                    remitentZipCode = "";
                    securedValue = 0;
                    securedValuesList.Clear();
                    serviceName = "";
                    status = "";
                    tracking = "";
                    trackingsList.Clear();
                    weight = 0;
                    weightsList.Clear();
                    width = 0;
                    widthsList.Clear();
                }
                xlWorksheet.Cells[1, 1] = "Reporte general de envios";
                xlWorksheet.Cells[2, 1] = "Numero de guia";
                xlWorksheet.Cells[2, 2] = "Numero de control";
                xlWorksheet.Cells[2, 3] = "Valor asegurado";
                xlWorksheet.Cells[2, 4] = "Operador";
                xlWorksheet.Cells[2, 5] = "Servicio";
                xlWorksheet.Cells[2, 6] = "Nombre del remitente";
                xlWorksheet.Cells[2, 7] = "Empresa del remitente";
                xlWorksheet.Cells[2, 8] = "Ciudad del remitente";
                xlWorksheet.Cells[2, 9] = "Codigo postal del remitente";
                xlWorksheet.Cells[2, 10] = "Estado del remitente";
                xlWorksheet.Cells[2, 11] = "Nombre del destinatario";
                xlWorksheet.Cells[2, 12] = "Ciudad del destinatario";
                xlWorksheet.Cells[2, 13] = "Codigo postal del destinatario";
                xlWorksheet.Cells[2, 14] = "Estado del destinatario";
                xlWorksheet.Cells[2, 15] = "Alto del envio";
                xlWorksheet.Cells[2, 16] = "Ancho del envio";
                xlWorksheet.Cells[2, 17] = "Largo del envio";
                xlWorksheet.Cells[2, 18] = "Contenido del envio";
                xlWorksheet.Cells[2, 19] = "Peso del envio";
                xlWorksheet.Cells[2, 20] = "Estatus del envio";
                xlWorksheet.Cells[2, 21] = "Fecha del envio";
                xlWorksheet.Cells[2, 22] = "ETA";
                xlWorksheet.Cells[2, 23] = "Comentarios";
                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteGeneralEnvios-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);

                string name = "ReporteGeneralEnvios-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email( name);

            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }

        }
        public void CreateBabbyReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'BASU%' ");
                    stringBuilder.Append("AND FechaCreacion > '2021-01-01'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesList.Add(reader[2].ToString());
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte de envios Babby Summer";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                int longitudLista = ShipmentsList.Count;
                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetApiData("Babby Summer", JsonObjectsList[i], ShipmentsList[i]);
                    status = ChangeStatusLanguage(status);
                    if (shipmentType == 0)
                    {
                        if (trackingsList.Count > 0)
                        {
                            for (int j = 0; j < trackingsList.Count; j++)
                            {
                                rowCount++;
                                xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                                xlWorksheet.Cells[rowCount, 1] = trackingsList[j];
                                if (j == 0)
                                    xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i];
                                else
                                    xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i] + "-" + j;
                                xlWorksheet.Cells[rowCount, 3] = operatorName;
                                xlWorksheet.Cells[rowCount, 4] = serviceName;
                                xlWorksheet.Cells[rowCount, 5] = remitentCity + ", " + remitentZipCode + ", " + remitentState + ", " + remitentCountry;
                                xlWorksheet.Cells[rowCount, 6] = remitentCompany;
                                xlWorksheet.Cells[rowCount, 7] = destinataryCity + ", " + destinataryZipCode + ", " + destinataryState + ", " + destinataryCountry;
                                xlWorksheet.Cells[rowCount, 8] = status;
                                xlWorksheet.Cells[rowCount, 9] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                                xlWorksheet.Cells[rowCount, 10] = heightsList[j];
                                xlWorksheet.Cells[rowCount, 11] = widthsList[j];
                                xlWorksheet.Cells[rowCount, 12] = lengthsList[j];
                                xlWorksheet.Cells[rowCount, 13] = weightsList[j];

                                Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                            }
                        }
                    }
                    else
                    {
                        if (tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = tracking;
                            xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i];
                            xlWorksheet.Cells[rowCount, 3] = operatorName;
                            xlWorksheet.Cells[rowCount, 4] = serviceName;
                            xlWorksheet.Cells[rowCount, 5] = remitentCity + ", " + remitentZipCode + ", " + remitentState + ", " + remitentCountry;
                            xlWorksheet.Cells[rowCount, 6] = remitentCompany;
                            xlWorksheet.Cells[rowCount, 7] = destinataryCity + ", " + destinataryZipCode + ", " + destinataryState + ", " + destinataryCountry;
                            xlWorksheet.Cells[rowCount, 8] = status;
                            xlWorksheet.Cells[rowCount, 9] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                            xlWorksheet.Cells[rowCount, 10] = height;
                            xlWorksheet.Cells[rowCount, 11] = width;
                            xlWorksheet.Cells[rowCount, 12] = length;
                            xlWorksheet.Cells[rowCount, 13] = weight;

                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                        }
                    }
                    destinataryCity = "";
                    destinataryCountry = "";
                    destinataryState = "";
                    destinataryZipCode = "";
                    operatorName = "";
                    remitentCity = "";
                    remitentCountry = "";
                    remitentCompany = "";
                    remitentState = "";
                    remitentZipCode = "";
                    serviceName = "";
                    status = "";
                    tracking = "";
                    trackingsList.Clear();
                    weight = 0;
                    weightsList.Clear();
                    width = 0;
                    widthsList.Clear();
                    length = 0;
                    lengthsList.Clear();
                    height = 0;
                    heightsList.Clear();

                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios Babby Summer";
                xlWorksheet.Cells[2, 1] = "Numero de guia";
                xlWorksheet.Cells[2, 2] = "Numero de control";
                xlWorksheet.Cells[2, 3] = "Operador";
                xlWorksheet.Cells[2, 4] = "Servicio";
                xlWorksheet.Cells[2, 5] = "Remitente";
                xlWorksheet.Cells[2, 6] = "Empresa del remitente";
                xlWorksheet.Cells[2, 7] = "Destinatario";
                xlWorksheet.Cells[2, 8] = "Estatus del envio";
                xlWorksheet.Cells[2, 9] = "Fecha del envio";

                xlWorksheet.Cells[2, 10] = "Alto";
                xlWorksheet.Cells[2, 11] = "Ancho";
                xlWorksheet.Cells[2, 12] = "Largo";
                xlWorksheet.Cells[2, 13] = "Peso";
                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteBabby-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);

                string name = "ReporteBabby-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email( name);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        public void CreateBaulReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'BACL%' ");
                    stringBuilder.Append("AND FechaCreacion > '2021-01-31'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesList.Add(reader[2].ToString());
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte de envios El Baul";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                int longitudLista = ShipmentsList.Count;

                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetApiData("El Baul de Cleo", JsonObjectsList[i], ShipmentsList[i]);
                    status = ChangeStatusLanguage(status);
                    if (shipmentType == 0)
                    {
                        for (int j = 0; j < trackingsList.Count; j++)
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = trackingsList[j];
                            if (j == 0)
                                xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i];
                            else
                                xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i] + "-" + j;
                            xlWorksheet.Cells[rowCount, 3] = operatorName;
                            xlWorksheet.Cells[rowCount, 4] = serviceName;
                            xlWorksheet.Cells[rowCount, 5] = remitentCity;
                            xlWorksheet.Cells[rowCount, 6] = remitentZipCode;
                            xlWorksheet.Cells[rowCount, 7] = remitentState;
                            xlWorksheet.Cells[rowCount, 8] = remitentCompany;
                            xlWorksheet.Cells[rowCount, 9] = destinataryCity;
                            xlWorksheet.Cells[rowCount, 10] = destinataryZipCode;
                            xlWorksheet.Cells[rowCount, 11] = destinataryState;
                            xlWorksheet.Cells[rowCount, 12] = status;
                            xlWorksheet.Cells[rowCount, 13] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                            xlWorksheet.Cells[rowCount, 14] = heightsList[j];
                            xlWorksheet.Cells[rowCount, 15] = widthsList[j];
                            xlWorksheet.Cells[rowCount, 16] = lengthsList[j];
                            xlWorksheet.Cells[rowCount, 17] = weightsList[j];

                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                        }
                    }
                    else
                    {
                        switch (ShipmentsList[i])
                        {
                            case "BACL2304202110003":
                                tracking = "392174233696";
                                status = "Entregado";
                                break;
                            case "BACL2304202110006":
                                tracking = "392174340142";
                                status = "Entregado";
                                break;
                            case "BACL2304202110007":
                                tracking = "392178593618";
                                status = "Entregado";
                                break;
                            case "BACL3004202110015":
                                tracking = "392411448735";
                                status = "Entregado";
                                break;
                            case "BACL0405202110018":
                                tracking = "392508480311";
                                status = "Entregado";
                                break;
                            case "BACL2405202110035":
                                tracking = "393175286280";
                                status = "Entregado";
                                break;
                            case "BACL2405202110037":
                                tracking = "393175330898";
                                status = "Entregado";
                                break;
                        }
                        if (tracking != null)
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = tracking;
                            xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i];
                            xlWorksheet.Cells[rowCount, 3] = operatorName;
                            xlWorksheet.Cells[rowCount, 4] = serviceName;
                            xlWorksheet.Cells[rowCount, 5] = remitentCity;
                            xlWorksheet.Cells[rowCount, 6] = remitentZipCode;
                            xlWorksheet.Cells[rowCount, 7] = remitentState;
                            xlWorksheet.Cells[rowCount, 8] = remitentCompany;
                            xlWorksheet.Cells[rowCount, 9] = destinataryCity;
                            xlWorksheet.Cells[rowCount, 10] = destinataryZipCode;
                            xlWorksheet.Cells[rowCount, 11] = destinataryState;
                            xlWorksheet.Cells[rowCount, 12] = status;
                            xlWorksheet.Cells[rowCount, 13] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                            xlWorksheet.Cells[rowCount, 14] = height;
                            xlWorksheet.Cells[rowCount, 15] = width;
                            xlWorksheet.Cells[rowCount, 16] = length;
                            xlWorksheet.Cells[rowCount, 17] = weight;

                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                        }
                    }
                    destinataryCity = "";
                    destinataryState = "";
                    destinataryZipCode = "";
                    operatorName = "";
                    remitentCity = "";
                    remitentState = "";
                    remitentZipCode = "";
                    remitentCompany = "";
                    serviceName = "";
                    status = "";
                    tracking = "";
                    trackingsList.Clear();

                    weight = 0;
                    weightsList.Clear();
                    width = 0;
                    widthsList.Clear();
                    length = 0;
                    lengthsList.Clear();
                    height = 0;
                    heightsList.Clear();

                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios El Baul de Cleo";
                xlWorksheet.Cells[2, 1] = "Numero de guia";
                xlWorksheet.Cells[2, 2] = "Numero de control";
                xlWorksheet.Cells[2, 3] = "Operador";
                xlWorksheet.Cells[2, 4] = "Servicio";
                xlWorksheet.Cells[2, 5] = "Ciudad del remitente";
                xlWorksheet.Cells[2, 6] = "Codigo postal del remitente";
                xlWorksheet.Cells[2, 7] = "Estado del remitente";
                xlWorksheet.Cells[2, 8] = "Empresa del remitente";
                xlWorksheet.Cells[2, 9] = "Ciudad del destinatario";
                xlWorksheet.Cells[2, 10] = "Codigo postal del destinatario";
                xlWorksheet.Cells[2, 11] = "Estado del destinatario";
                xlWorksheet.Cells[2, 12] = "Estatus del envio";
                xlWorksheet.Cells[2, 13] = "Fecha del envio";

                xlWorksheet.Cells[2, 14] = "Alto";
                xlWorksheet.Cells[2, 15] = "Ancho";
                xlWorksheet.Cells[2, 16] = "Largo";
                xlWorksheet.Cells[2, 17] = "Peso";

                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteBaul-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);

                string name = "ReporteBaul-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email( name);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        public void CreateElementReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'ELEM%' ");
                    stringBuilder.Append("AND FechaCreacion > '2019-11-01'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesList.Add(reader[2].ToString());
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte de envios Element";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                int longitudLista = ShipmentsList.Count;

                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetApiData("Element SPA Line SA de CV", JsonObjectsList[i], ShipmentsList[i]);
                    status = ChangeStatusLanguage(status);
                    if (status != "Entregado"
                        && status != "Sin descripcion por FedEx"
                        && status != "Envio cancelado por el cliente"
                        && status != "Completo por Seguimiento"
                        && Convert.ToDateTime(CreationDatesList[i]).Year != 2018)
                    {
                        if (shipmentType == 0)
                        {
                            if (trackingsList.Count > 0)
                            {
                                for (int j = 0; j < trackingsList.Count; j++)
                                {
                                    rowCount++;
                                    xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                                    xlWorksheet.Cells[rowCount, 1] = trackingsList[j];
                                    if (j == 0)
                                        xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i];
                                    else
                                        xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i] + "-" + j;
                                    xlWorksheet.Cells[rowCount, 3] = securedValuesList[j];
                                    xlWorksheet.Cells[rowCount, 4] = operatorName;
                                    xlWorksheet.Cells[rowCount, 5] = serviceName;
                                    xlWorksheet.Cells[rowCount, 6] = remitentName;
                                    xlWorksheet.Cells[rowCount, 7] = remitentCity;
                                    xlWorksheet.Cells[rowCount, 8] = remitentZipCode;
                                    xlWorksheet.Cells[rowCount, 9] = remitentState;
                                    xlWorksheet.Cells[rowCount, 10] = remitentCompany;
                                    xlWorksheet.Cells[rowCount, 11] = destinataryName;
                                    xlWorksheet.Cells[rowCount, 12] = destinataryCity;
                                    xlWorksheet.Cells[rowCount, 13] = destinataryZipCode;
                                    xlWorksheet.Cells[rowCount, 14] = destinataryState;
                                    xlWorksheet.Cells[rowCount, 15] = heightsList[j];
                                    xlWorksheet.Cells[rowCount, 16] = widthsList[j];
                                    xlWorksheet.Cells[rowCount, 17] = lengthsList[j];
                                    xlWorksheet.Cells[rowCount, 18] = content;
                                    xlWorksheet.Cells[rowCount, 19] = weightsList[j];
                                    xlWorksheet.Cells[rowCount, 20] = status;
                                    xlWorksheet.Cells[rowCount, 21] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");

                                    Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");

                                }
                            }
                        }
                        else
                        {
                            if (tracking != "")
                            {
                                rowCount++;
                                xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                                xlWorksheet.Cells[rowCount, 1] = tracking;
                                xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i];
                                xlWorksheet.Cells[rowCount, 3] = securedValue;
                                xlWorksheet.Cells[rowCount, 4] = operatorName;
                                xlWorksheet.Cells[rowCount, 5] = serviceName;
                                xlWorksheet.Cells[rowCount, 6] = remitentName;
                                xlWorksheet.Cells[rowCount, 7] = remitentCity;
                                xlWorksheet.Cells[rowCount, 8] = remitentZipCode;
                                xlWorksheet.Cells[rowCount, 9] = remitentState;
                                xlWorksheet.Cells[rowCount, 10] = remitentCompany;
                                xlWorksheet.Cells[rowCount, 11] = destinataryName;
                                xlWorksheet.Cells[rowCount, 12] = destinataryCity;
                                xlWorksheet.Cells[rowCount, 13] = destinataryZipCode;
                                xlWorksheet.Cells[rowCount, 14] = destinataryState;
                                xlWorksheet.Cells[rowCount, 15] = height;
                                xlWorksheet.Cells[rowCount, 16] = width;
                                xlWorksheet.Cells[rowCount, 17] = length;
                                xlWorksheet.Cells[rowCount, 18] = content;
                                xlWorksheet.Cells[rowCount, 19] = weight;
                                xlWorksheet.Cells[rowCount, 20] = status;
                                xlWorksheet.Cells[rowCount, 21] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");

                                Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                            }
                        }
                    }
                    content = "";
                    destinataryCity = "";
                    destinataryName = "";
                    destinataryState = "";
                    destinataryZipCode = "";
                    height = 0;
                    heightsList.Clear();
                    length = 0;
                    lengthsList.Clear();
                    operatorName = "";
                    remitentCity = "";
                    remitentName = "";
                    remitentState = "";
                    remitentCompany = "";
                    remitentZipCode = "";
                    securedValue = 0;
                    securedValuesList.Clear();
                    serviceName = "";
                    status = "";
                    tracking = "";
                    trackingsList.Clear();
                    weight = 0;
                    weightsList.Clear();
                    width = 0;
                    widthsList.Clear();
                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios Element";
                xlWorksheet.Cells[2, 1] = "Numero de guia";
                xlWorksheet.Cells[2, 2] = "Numero de control";
                xlWorksheet.Cells[2, 3] = "Valor asegurado";
                xlWorksheet.Cells[2, 4] = "Operador";
                xlWorksheet.Cells[2, 5] = "Servicio";
                xlWorksheet.Cells[2, 6] = "Nombre del remitente";
                xlWorksheet.Cells[2, 7] = "Ciudad del remitente";
                xlWorksheet.Cells[2, 8] = "Codigo postal del remitente";
                xlWorksheet.Cells[2, 9] = "Estado del remitente";
                xlWorksheet.Cells[2, 10] = "Empresa del remitente";
                xlWorksheet.Cells[2, 11] = "Nombre del destinatario";
                xlWorksheet.Cells[2, 12] = "Ciudad del destinatario";
                xlWorksheet.Cells[2, 13] = "Codigo postal del destinatario";
                xlWorksheet.Cells[2, 14] = "Estado del destinatario";
                xlWorksheet.Cells[2, 15] = "Alto del envio";
                xlWorksheet.Cells[2, 16] = "Ancho del envio";
                xlWorksheet.Cells[2, 17] = "Largo del envio";
                xlWorksheet.Cells[2, 18] = "Contenido del envio";
                xlWorksheet.Cells[2, 19] = "Peso del envio";
                xlWorksheet.Cells[2, 20] = "Estatus del envio";
                xlWorksheet.Cells[2, 21] = "Fecha del envio";
                xlWorksheet.Cells[2, 22] = "ETA";
                xlWorksheet.Cells[2, 23] = "Comentarios";
                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteElement-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);

                string name = "ReporteElement-" +
                   DateTime.Today.Year.ToString() + "-" +
                   DateTime.Today.Month.ToString() + "-" +
                   DateTime.Today.Day.ToString();

                Email( name);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        public void CreateGameSmartReport()
        {
            List<string> JsonObjectsList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'GASM%' ");
                    stringBuilder.Append("AND FechaCreacion > '2021-01-01'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte de envios Game Smart";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                int longitudLista = ShipmentsList.Count;

                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetApiData("GameSmart de Mexico, SA de CV", JsonObjectsList[i], ShipmentsList[i]);
                    status = ChangeStatusLanguage(status);
                    if (shipmentType == 0)
                    {
                        for (int j = 0; j < trackingsList.Count; j++)
                        {
                            rowCount++;
                            if (j == 0)
                                xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i];
                            else
                                xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i] + "-" + j;
                            xlWorksheet.Cells[rowCount, 2].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 2] = trackingsList[j];
                            xlWorksheet.Cells[rowCount, 3] = remitentCompany;
                            xlWorksheet.Cells[rowCount, 4] = destinataryName;
                            xlWorksheet.Cells[rowCount, 5] = status;

                            xlWorksheet.Cells[rowCount, 6] = heightsList[j];
                            xlWorksheet.Cells[rowCount, 7] = widthsList[j];
                            xlWorksheet.Cells[rowCount, 8] = lengthsList[j];
                            xlWorksheet.Cells[rowCount, 9] = weightsList[j];

                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                        }
                    }
                    else
                    {
                        if (tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i];
                            xlWorksheet.Cells[rowCount, 2].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 2] = tracking;
                            xlWorksheet.Cells[rowCount, 3] = remitentCompany;
                            xlWorksheet.Cells[rowCount, 4] = destinataryName;
                            xlWorksheet.Cells[rowCount, 5] = status;
                            xlWorksheet.Cells[rowCount, 6] = height;
                            xlWorksheet.Cells[rowCount, 7] = width;
                            xlWorksheet.Cells[rowCount, 8] = length;
                            xlWorksheet.Cells[rowCount, 9] = weight;


                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                        }
                    }
                    destinataryName = "";
                    status = "";
                    tracking = "";
                    trackingsList.Clear();

                    weight = 0;
                    weightsList.Clear();
                    width = 0;
                    widthsList.Clear();
                    length = 0;
                    lengthsList.Clear();
                    height = 0;
                    heightsList.Clear();
                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios Game Smart";
                xlWorksheet.Cells[2, 1] = "Numero de control";
                xlWorksheet.Cells[2, 2] = "Numero de guia";
                xlWorksheet.Cells[2, 3] = "Empresa del Remitente";
                xlWorksheet.Cells[2, 4] = "Nombre del destinatario";
                xlWorksheet.Cells[2, 5] = "Estatus";
                xlWorksheet.Cells[2, 6] = "Alto";
                xlWorksheet.Cells[2, 7] = "Ancho";
                xlWorksheet.Cells[2, 8] = "Largo";
                xlWorksheet.Cells[2, 9] = "Peso";
                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteGameSmart-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                string name = "ReporteGameSmart-" +
                   DateTime.Today.Year.ToString() + "-" +
                   DateTime.Today.Month.ToString() + "-" +
                   DateTime.Today.Day.ToString();

                Email(name);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        public void CreateJesyReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'JEAL%' ");
                    stringBuilder.Append("AND FechaCreacion > '2021-01-01'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesList.Add(reader[2].ToString());
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte de envios Jesy Almaguer";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                int longitudLista = ShipmentsList.Count;

                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetApiData("JESY ALMAGUER", JsonObjectsList[i], ShipmentsList[i]);
                    status = ChangeStatusLanguage(status);
                    if (shipmentType == 0)
                    {
                        if (trackingsList.Count > 0)
                        {
                            for (int j = 0; j < trackingsList.Count; j++)
                            {
                                rowCount++;
                                xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                                if (j == 0)
                                    xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i].Substring(14, 3);
                                else
                                    xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i].Substring(14, 3) + "-" + j;
                                xlWorksheet.Cells[rowCount, 2] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                                xlWorksheet.Cells[rowCount, 3] = remitentName;
                                xlWorksheet.Cells[rowCount, 4] = remitentCompany;
                                xlWorksheet.Cells[rowCount, 5] = destinataryName;
                                xlWorksheet.Cells[rowCount, 6] = operatorName;
                                xlWorksheet.Cells[rowCount, 7] = serviceName;
                                xlWorksheet.Cells[rowCount, 8].NumberFormat = "@";
                                xlWorksheet.Cells[rowCount, 8] = trackingsList[j];
                                xlWorksheet.Cells[rowCount, 9] = status;

                                xlWorksheet.Cells[rowCount, 10] = heightsList[j];
                                xlWorksheet.Cells[rowCount, 11] = widthsList[j];
                                xlWorksheet.Cells[rowCount, 12] = lengthsList[j];
                                xlWorksheet.Cells[rowCount, 13] = weightsList[j];

                                Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                            }
                        }
                    }
                    else
                    {
                        if (tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i].Substring(14, 3);
                            xlWorksheet.Cells[rowCount, 2] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                            xlWorksheet.Cells[rowCount, 3] = remitentName;
                            xlWorksheet.Cells[rowCount, 4] = remitentCompany;
                            xlWorksheet.Cells[rowCount, 5] = destinataryName;
                            xlWorksheet.Cells[rowCount, 6] = operatorName;
                            xlWorksheet.Cells[rowCount, 7] = serviceName;
                            xlWorksheet.Cells[rowCount, 8].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 8] = tracking;
                            xlWorksheet.Cells[rowCount, 9] = status;
                            xlWorksheet.Cells[rowCount, 10] = height;
                            xlWorksheet.Cells[rowCount, 11] = width;
                            xlWorksheet.Cells[rowCount, 12] = length;
                            xlWorksheet.Cells[rowCount, 13] = weight;


                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                        }
                    }
                    destinataryCity = "";
                    destinataryCountry = "";
                    destinataryState = "";
                    destinataryZipCode = "";
                    operatorName = "";
                    remitentCity = "";
                    remitentCompany = "";
                    remitentCountry = "";
                    remitentState = "";
                    remitentZipCode = "";
                    serviceName = "";
                    status = "";
                    tracking = "";
                    trackingsList.Clear();

                    weight = 0;
                    weightsList.Clear();
                    width = 0;
                    widthsList.Clear();
                    length = 0;
                    lengthsList.Clear();
                    height = 0;
                    heightsList.Clear();

                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios Jesy Almaguer";
                xlWorksheet.Cells[2, 1] = "Folio";
                xlWorksheet.Cells[2, 2] = "Fecha de envio";
                xlWorksheet.Cells[2, 3] = "Remitente";
                xlWorksheet.Cells[2, 4] = "Empresa del remitente";
                xlWorksheet.Cells[2, 5] = "Destinatario";
                xlWorksheet.Cells[2, 6] = "Operador";
                xlWorksheet.Cells[2, 7] = "Servicio";
                xlWorksheet.Cells[2, 8] = "Guia";
                xlWorksheet.Cells[2, 9] = "Estatus del envio";

                xlWorksheet.Cells[2, 10] = "Alto";
                xlWorksheet.Cells[2, 11] = "Ancho";
                xlWorksheet.Cells[2, 12] = "Largo";
                xlWorksheet.Cells[2, 13] = "Peso";

                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteJesy-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                string name = "ReporteJesy-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email( name);

            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        public void CreateTecPlussReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'TEPL%' ");
                    stringBuilder.Append("AND FechaCreacion > '2021-01-01'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesList.Add(reader[2].ToString());
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte de envios Tec Pluss";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                int longitudLista = ShipmentsList.Count;

                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetApiData("TEC PLUSS", JsonObjectsList[i], ShipmentsList[i]);
                    status = ChangeStatusLanguage(status);
                    if (shipmentType == 0)
                    {
                        if (trackingsList.Count > 0)
                        {
                            for (int j = 0; j < trackingsList.Count; j++)
                            {
                                rowCount++;
                                xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                                if (j == 0)
                                    xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i].Substring(14, 3);
                                else
                                    xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i].Substring(14, 3) + "-" + j;
                                xlWorksheet.Cells[rowCount, 2] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                                xlWorksheet.Cells[rowCount, 3].NumberFormat = "@";
                                xlWorksheet.Cells[rowCount, 3] = trackingsList[j];
                                xlWorksheet.Cells[rowCount, 4] = operatorName;
                                xlWorksheet.Cells[rowCount, 5] = serviceName;
                                xlWorksheet.Cells[rowCount, 6] = remitentCompany;
                                xlWorksheet.Cells[rowCount, 7] = destinataryName;
                                xlWorksheet.Cells[rowCount, 8] = status;

                                xlWorksheet.Cells[rowCount, 9] = heightsList[j];
                                xlWorksheet.Cells[rowCount, 10] = widthsList[j];
                                xlWorksheet.Cells[rowCount, 11] = lengthsList[j];
                                xlWorksheet.Cells[rowCount, 12] = weightsList[j];

                                Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                            }
                        }
                    }
                    else
                    {
                        if (tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i].Substring(14, 3);
                            xlWorksheet.Cells[rowCount, 2] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                            xlWorksheet.Cells[rowCount, 3].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 3] = tracking;
                            xlWorksheet.Cells[rowCount, 4] = operatorName;
                            xlWorksheet.Cells[rowCount, 5] = serviceName;
                            xlWorksheet.Cells[rowCount, 6] = remitentCompany;
                            xlWorksheet.Cells[rowCount, 7] = destinataryName;
                            xlWorksheet.Cells[rowCount, 8] = status;

                            xlWorksheet.Cells[rowCount, 9] = height;
                            xlWorksheet.Cells[rowCount, 10] = width;
                            xlWorksheet.Cells[rowCount, 11] = length;
                            xlWorksheet.Cells[rowCount, 12] = weight;

                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                        }
                    }
                    destinataryCity = "";
                    destinataryCountry = "";
                    destinataryState = "";
                    destinataryZipCode = "";
                    operatorName = "";
                    remitentCity = "";
                    remitentCountry = "";
                    remitentState = "";
                    remitentZipCode = "";
                    remitentCompany = "";
                    serviceName = "";
                    status = "";
                    tracking = "";
                    trackingsList.Clear();
                    weight = 0;
                    weightsList.Clear();
                    width = 0;
                    widthsList.Clear();
                    length = 0;
                    lengthsList.Clear();
                    height = 0;
                    heightsList.Clear();
                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios Tec Pluss";
                xlWorksheet.Cells[2, 1] = "Folio";
                xlWorksheet.Cells[2, 2] = "Fecha de envio";
                xlWorksheet.Cells[2, 3] = "Guia";
                xlWorksheet.Cells[2, 4] = "Operador";
                xlWorksheet.Cells[2, 5] = "Servicio";
                xlWorksheet.Cells[2, 6] = "Empresa del remitente";
                xlWorksheet.Cells[2, 7] = "Destinatario";
                xlWorksheet.Cells[2, 8] = "Estatus del envio";
                xlWorksheet.Cells[2, 9] = "Alto";
                xlWorksheet.Cells[2, 10] = "Ancho";
                xlWorksheet.Cells[2, 11] = "Largo";
                xlWorksheet.Cells[2, 12] = "Peso";
                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteTecPluss-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                string name = "ReporteTecPluss-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email(name);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        public void CreateLikkusReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionEnvios ");
                    stringBuilder.Append("WHERE NumeroControl LIKE 'LIKK%' ");
                    stringBuilder.Append("AND FechaCreacion > '2021-01-01'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesList.Add(reader[2].ToString());
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte de envios Likkus";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                int longitudLista = ShipmentsList.Count;

                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetLikkusData(JsonObjectsList[i]);
                    rowCount++;
                    xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i];
                    xlWorksheet.Cells[rowCount, 2] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                    xlWorksheet.Cells[rowCount, 6] = order;

                    Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");

                    order = "";
                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios Likkus";
                xlWorksheet.Cells[2, 1] = "Numero de control";
                xlWorksheet.Cells[2, 2] = "Fecha del envio";
                xlWorksheet.Cells[2, 3] = "# guia pallet";
                xlWorksheet.Cells[2, 4] = "Estatus";
                xlWorksheet.Cells[2, 5] = "Fecha de entrega";
                xlWorksheet.Cells[2, 6] = "Sucursal Liverpool";
                xlWorksheet.Cells[2, 7] = "Guia de retorno";
                xlWorksheet.Cells[2, 8] = "Estatus 2";
                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteLikkus-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                string name = "ReporteLikkus-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email(name);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        public void CreateOsmagReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'OSMA%' OR NumeroControl LIKE 'OSSA%'");
                    stringBuilder.Append("AND FechaCreacion > '2021-01-01'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesList.Add(reader[2].ToString());
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte de envios OSMAG";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                int longitudLista = ShipmentsList.Count;

                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    if(i > 1100)
                    {
                        Console.WriteLine(".");
                    }
                    GetApiData("OSMAG", JsonObjectsList[i], ShipmentsList[i]);
                    status = ChangeStatusLanguage(status);
                    if (shipmentType == 0)
                    {
                        if (trackingsList.Count > 0)
                        {
                            for (int j = 0; j < trackingsList.Count; j++)
                            {
                                rowCount++;
                                xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                                if (j == 0)
                                    xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i].Substring(14, 3);
                                else
                                    xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i].Substring(14, 3) + "-" + j;
                                xlWorksheet.Cells[rowCount, 1] = remitentName;
                                xlWorksheet.Cells[rowCount, 2] = remitentCompany;
                                xlWorksheet.Cells[rowCount, 3] = destinataryCompany;
                                xlWorksheet.Cells[rowCount, 4] = serviceName;
                                xlWorksheet.Cells[rowCount, 5].NumberFormat = "@";
                                xlWorksheet.Cells[rowCount, 5] = trackingsList[j];
                                xlWorksheet.Cells[rowCount, 6] = status;
                                xlWorksheet.Cells[rowCount, 7] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                                xlWorksheet.Cells[rowCount, 8] = operatorName;
                                xlWorksheet.Cells[rowCount, 9] = heightsList[j];
                                xlWorksheet.Cells[rowCount, 10] = widthsList[j];
                                xlWorksheet.Cells[rowCount, 11] = lengthsList[j];
                                xlWorksheet.Cells[rowCount, 12] = weightsList[j];

                                Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                            }
                        }
                    }
                    else
                    {
                        if (tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = remitentName;
                            xlWorksheet.Cells[rowCount, 2] = remitentCompany;
                            xlWorksheet.Cells[rowCount, 3] = destinataryCompany;
                            xlWorksheet.Cells[rowCount, 4] = serviceName;
                            xlWorksheet.Cells[rowCount, 5].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 5] = tracking;
                            xlWorksheet.Cells[rowCount, 6] = status;
                            xlWorksheet.Cells[rowCount, 7] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                            xlWorksheet.Cells[rowCount, 8] = operatorName;
                            xlWorksheet.Cells[rowCount, 9] = height;
                            xlWorksheet.Cells[rowCount, 10] = width;
                            xlWorksheet.Cells[rowCount, 11] = length;
                            xlWorksheet.Cells[rowCount, 12] = weight;
                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                        }
                    
                    }
                    destinataryCity = "";
                    destinataryCountry = "";
                    destinataryState = "";
                    destinataryZipCode = "";
                    operatorName = "";
                    remitentCity = "";
                    remitentCompany = "";
                    remitentCountry = "";
                    remitentState = "";
                    remitentZipCode = "";
                    serviceName = "";
                    status = "";
                    tracking = "";
                    trackingsList.Clear();

                    weight = 0;
                    weightsList.Clear();
                    width = 0;
                    widthsList.Clear();
                    length = 0;
                    lengthsList.Clear();
                    height = 0;
                    heightsList.Clear();
                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios OSMAG";
                xlWorksheet.Cells[2, 1] = "Origen";
                xlWorksheet.Cells[2, 2] = "Empresa del remitente";
                xlWorksheet.Cells[2, 3] = "Destino";
                xlWorksheet.Cells[2, 4] = "Servicio";
                xlWorksheet.Cells[2, 5] = "Guia";
                xlWorksheet.Cells[2, 6] = "Estatus del envio";
                xlWorksheet.Cells[2, 7] = "Fecha";
                xlWorksheet.Cells[2, 8] = "Operador";
                xlWorksheet.Cells[2, 9] = "Alto";
                xlWorksheet.Cells[2, 10] = "Ancho";
                xlWorksheet.Cells[2, 11] = "Largo";
                xlWorksheet.Cells[2, 12] = "Peso";
                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteOsmag-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                string name = "ReporteOsmag-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email(name);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        public void CreatePsicofarmaReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'PSIC%' ");
                    stringBuilder.Append("AND FechaCreacion > '2021-01-31'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesList.Add(reader[2].ToString());
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte de envios Psicofarma";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                int longitudLista = ShipmentsList.Count;

                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetApiData("Psicofarma, SA de CV", JsonObjectsList[i], ShipmentsList[i]);
                    status = ChangeStatusLanguage(status);
                    if (userName == "Verenice Guevara")
                    {
                        if (shipmentType == 0)
                        {
                            if (trackingsList.Count > 0)
                            {
                                for (int j = 0; j < trackingsList.Count; j++)
                                {
                                    rowCount++;
                                    xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                                    xlWorksheet.Cells[rowCount, 1] = trackingsList[j];
                                    if (j == 0)
                                        xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i];
                                    else
                                        xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i] + "-" + j;
                                    xlWorksheet.Cells[rowCount, 3] = securedValuesList[j];
                                    xlWorksheet.Cells[rowCount, 4] = operatorName;
                                    xlWorksheet.Cells[rowCount, 5] = serviceName;
                                    xlWorksheet.Cells[rowCount, 6] = remitentCompany;
                                    xlWorksheet.Cells[rowCount, 7] = remitentName;
                                    xlWorksheet.Cells[rowCount, 8] = remitentCity;
                                    xlWorksheet.Cells[rowCount, 9] = remitentZipCode;
                                    xlWorksheet.Cells[rowCount, 10] = remitentState;
                                    xlWorksheet.Cells[rowCount, 11] = destinataryName;
                                    xlWorksheet.Cells[rowCount, 12] = destinataryCity;
                                    xlWorksheet.Cells[rowCount, 13] = destinataryZipCode;
                                    xlWorksheet.Cells[rowCount, 14] = destinataryState;
                                    xlWorksheet.Cells[rowCount, 15] = heightsList[j];
                                    xlWorksheet.Cells[rowCount, 16] = widthsList[j];
                                    xlWorksheet.Cells[rowCount, 17] = lengthsList[j];
                                    xlWorksheet.Cells[rowCount, 18] = content;
                                    xlWorksheet.Cells[rowCount, 19] = weightsList[j];
                                    xlWorksheet.Cells[rowCount, 20] = status;
                                    xlWorksheet.Cells[rowCount, 21] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");

                                    Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                                }
                            }
                        }
                        else
                        {
                            if (tracking != "")
                            {
                                rowCount++;
                                xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                                xlWorksheet.Cells[rowCount, 1] = tracking;
                                xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i];
                                xlWorksheet.Cells[rowCount, 3] = securedValue;
                                xlWorksheet.Cells[rowCount, 4] = operatorName;
                                xlWorksheet.Cells[rowCount, 5] = serviceName;
                                xlWorksheet.Cells[rowCount, 6] = remitentCompany;
                                xlWorksheet.Cells[rowCount, 7] = remitentName;
                                xlWorksheet.Cells[rowCount, 8] = remitentCity;
                                xlWorksheet.Cells[rowCount, 9] = remitentZipCode;
                                xlWorksheet.Cells[rowCount, 10] = remitentState;
                                xlWorksheet.Cells[rowCount, 11] = destinataryName;
                                xlWorksheet.Cells[rowCount, 12] = destinataryCity;
                                xlWorksheet.Cells[rowCount, 13] = destinataryZipCode;
                                xlWorksheet.Cells[rowCount, 14] = destinataryState;
                                xlWorksheet.Cells[rowCount, 15] = height;
                                xlWorksheet.Cells[rowCount, 16] = width;
                                xlWorksheet.Cells[rowCount, 17] = length;
                                xlWorksheet.Cells[rowCount, 18] = content;
                                xlWorksheet.Cells[rowCount, 19] = weight;
                                xlWorksheet.Cells[rowCount, 20] = status;
                                xlWorksheet.Cells[rowCount, 21] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");

                                Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                            }
                        }
                    }
                    content = "";
                    destinataryCity = "";
                    destinataryName = "";
                    destinataryState = "";
                    destinataryZipCode = "";
                    height = 0;
                    heightsList.Clear();
                    length = 0;
                    lengthsList.Clear();
                    operatorName = "";
                    remitentCity = "";
                    remitentCompany = "";
                    remitentName = "";
                    remitentState = "";
                    remitentZipCode = "";
                    securedValue = 0;
                    securedValuesList.Clear();
                    serviceName = "";
                    status = "";
                    tracking = "";
                    trackingsList.Clear();
                    weight = 0;
                    weightsList.Clear();
                    width = 0;
                    widthsList.Clear();
                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios Psicofarma";
                xlWorksheet.Cells[2, 1] = "Numero de guia";
                xlWorksheet.Cells[2, 2] = "Numero de control";
                xlWorksheet.Cells[2, 3] = "Valor asegurado";
                xlWorksheet.Cells[2, 4] = "Operador";
                xlWorksheet.Cells[2, 5] = "Servicio";
                xlWorksheet.Cells[2, 6] = "Empresa del remitente";
                xlWorksheet.Cells[2, 7] = "Nombre del remitente";
                xlWorksheet.Cells[2, 8] = "Ciudad del remitente";
                xlWorksheet.Cells[2, 9] = "Codigo postal del remitente";
                xlWorksheet.Cells[2, 10] = "Estado del remitente";
                xlWorksheet.Cells[2, 11] = "Nombre del destinatario";
                xlWorksheet.Cells[2, 12] = "Ciudad del destinatario";
                xlWorksheet.Cells[2, 13] = "Codigo postal del destinatario";
                xlWorksheet.Cells[2, 14] = "Estado del destinatario";
                xlWorksheet.Cells[2, 15] = "Alto del envio";
                xlWorksheet.Cells[2, 16] = "Ancho del envio";
                xlWorksheet.Cells[2, 17] = "Largo del envio";
                xlWorksheet.Cells[2, 18] = "Contenido del envio";
                xlWorksheet.Cells[2, 19] = "Peso del envio";
                xlWorksheet.Cells[2, 20] = "Estatus del envio";
                xlWorksheet.Cells[2, 21] = "Fecha del envio";
                xlWorksheet.Cells[2, 22] = "ETA";
                xlWorksheet.Cells[2, 23] = "Comentarios";
                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReportePsicofarma-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                string name = "ReportePsicofarma-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email(name);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }

        public void CreateRAGAReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> CreationDatesApiList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> JsonObjectsApiList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            List<string> ShipmentsApiList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'RAGA%' ");
                    stringBuilder.Append("AND FechaCreacion > '2018-12-31'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesApiList.Add(reader[2].ToString());
                                JsonObjectsApiList.Add(reader[3].ToString());
                                ShipmentsApiList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'RAGA%' ");
                    stringBuilder.Append("AND FechaCreacion > '2018-12-31'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesList.Add(reader[2].ToString());
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte de envios RAGA";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                for (int i = 0; i < ShipmentsApiList.Count; i++)
                {
                    GetRAGAApiData(JsonObjectsApiList[i]);
                    status = ChangeStatusLanguage(status);
                    if (status != "Entregado"
                        && status != "Sin descripcion por FedEx"
                        && status != "Envio cancelado por el cliente"
                        && status != "Completo por Seguimiento"
                        && Convert.ToDateTime(CreationDatesApiList[i]) > DateTime.Now.AddDays(-30))
                    {
                        if (tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = tracking;
                            xlWorksheet.Cells[rowCount, 2] = ShipmentsApiList[i];
                            xlWorksheet.Cells[rowCount, 3].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 3] = order;
                            xlWorksheet.Cells[rowCount, 4] = destinataryName;
                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + ", espere un momento por favor...");
                        }
                    }
                    tracking = "";
                    order = "";
                    destinataryName = "";
                }
                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetRAGAData(JsonObjectsList[i], ShipmentsList[i]);
                    status = ChangeStatusLanguage(status);
                    if (status != "Entregado"
                        && status != "Sin descripcion por FedEx"
                        && status != "Envio cancelado por el cliente"
                        && status != "Completo por Seguimiento"
                        && Convert.ToDateTime(CreationDatesList[i]) > DateTime.Now.AddDays(-30))
                    {
                        if (tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = tracking;
                            xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i];
                            xlWorksheet.Cells[rowCount, 3].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 3] = order;
                            xlWorksheet.Cells[rowCount, 4] = destinataryName;
                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + ", espere un momento por favor...");
                        }
                    }
                    tracking = "";
                    order = "";
                    destinataryName = "";
                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios RAGA";
                xlWorksheet.Cells[2, 1] = "Numero de guia";
                xlWorksheet.Cells[2, 2] = "Numero de control";
                xlWorksheet.Cells[2, 3] = "Numero de orden";
                xlWorksheet.Cells[2, 4] = "Nombre del cliente";
                xlWorksheet.Cells[2, 5] = "Comentarios";
                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteRAGA-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                string name = "ReporteRAGA-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email(name);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        public void CreateEstafetaReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> CreationDatesApiList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> JsonObjectsApiList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            List<string> ShipmentsApiList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT FechaCreacion, ObjetoRequest,NumeroControl ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");

                    stringBuilder.Append("where ObjetoRequest like '%");
                    stringBuilder.Append("\"TipoCourier\":4,%'");
                    //stringBuilder.Append("AND NumeroControl LIKE 'RAGA%' ");
                    //FechaAtendido >= '2022-07-01 00:00:00.000'
                    stringBuilder.Append("AND FechaCreacion > '2022-07-01 00:00:00.000'");
                    String stringResult = stringBuilder.ToString();

                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesApiList.Add(reader[0].ToString());
                                JsonObjectsApiList.Add(reader[1].ToString());
                                ShipmentsApiList.Add(reader[2].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
               
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte de envios Estafeta";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                for (int i = 0; i < ShipmentsApiList.Count; i++)
                {
                    GetEstafetaData(JsonObjectsApiList[i]);
                    status = ChangeStatusLanguage(status);
                    if (status != "Entregado"
                        && status != "Sin descripcion por FedEx"
                        && status != "Envio cancelado por el cliente"
                        && status != "Completo por Seguimiento")
                        //&& Convert.ToDateTime(CreationDatesApiList[i]) > DateTime.Now.AddDays(-30))
                    {
                        if (tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = tracking;
                            xlWorksheet.Cells[rowCount, 2] = ShipmentsApiList[i];
                            xlWorksheet.Cells[rowCount, 3].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 3] = order;
                            xlWorksheet.Cells[rowCount, 4] = destinataryName;
                            //xlWorksheet.Cells[rowCount, 5] = tipoCourier;
                            xlWorksheet.Cells[rowCount, 5] = CreationDatesApiList[i];
                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + ", espere un momento por favor...");
                        }
                    }
                    tracking = "";
                    order = "";
                    destinataryName = "";
                }
                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetRAGAData(JsonObjectsList[i], ShipmentsList[i]);
                    status = ChangeStatusLanguage(status);
                    if (status != "Entregado"
                        && status != "Sin descripcion por FedEx"
                        && status != "Envio cancelado por el cliente"
                        && status != "Completo por Seguimiento"
                        && Convert.ToDateTime(CreationDatesList[i]) > DateTime.Now.AddDays(-30))
                    {
                        if (tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = tracking;
                            xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i];
                            xlWorksheet.Cells[rowCount, 3].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 3] = order;
                            xlWorksheet.Cells[rowCount, 4] = destinataryName;
                            xlWorksheet.Cells[rowCount, 5] = CreationDatesApiList[i];
                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + ", espere un momento por favor...");
                        }
                    }
                    tracking = "";
                    order = "";
                    destinataryName = "";
                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios Estafeta";
                xlWorksheet.Cells[2, 1] = "Numero de guia";
                xlWorksheet.Cells[2, 2] = "Numero de control";
                xlWorksheet.Cells[2, 3] = "Numero de orden";
                xlWorksheet.Cells[2, 4] = "Nombre del cliente";
                xlWorksheet.Cells[2, 5] = "Fecha Creacion";
                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteEstafeta-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                string name = "ReporteEstafeta-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email(name);
            }
            catch (SqlException e)
            {
                Console.WriteLine(" Reporte Estafeta Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }

        //--------------------------
        public void CreateRedpackReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> CreationDatesApiList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> JsonObjectsApiList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            List<string> ShipmentsApiList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT FechaCreacion, ObjetoRequest,NumeroControl ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");

                    stringBuilder.Append("where ObjetoRequest like '%");
                    stringBuilder.Append("\"TipoCourier\":1,%'");
                    //stringBuilder.Append("AND NumeroControl LIKE 'RAGA%' ");
                    //FechaAtendido >= '2022-07-01 00:00:00.000'
                    stringBuilder.Append("AND FechaCreacion > '2022-07-01 00:00:00.000'");
                    String stringResult = stringBuilder.ToString();

                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesApiList.Add(reader[0].ToString());
                                JsonObjectsApiList.Add(reader[1].ToString());
                                ShipmentsApiList.Add(reader[2].ToString());
                            }
                        }
                    }
                    connection.Close();
                }

                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte de envios Estafeta";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                for (int i = 0; i < ShipmentsApiList.Count; i++)
                {
                    GetEstafetaData(JsonObjectsApiList[i]);
                    status = ChangeStatusLanguage(status);
                    if (status != "Entregado"
                        && status != "Sin descripcion por FedEx"
                        && status != "Envio cancelado por el cliente"
                        && status != "Completo por Seguimiento"
                        && Convert.ToDateTime(CreationDatesApiList[i]) > DateTime.Now.AddDays(-30))
                    {
                        if (tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = tracking;
                            xlWorksheet.Cells[rowCount, 2] = ShipmentsApiList[i];
                            xlWorksheet.Cells[rowCount, 3].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 3] = order;
                            xlWorksheet.Cells[rowCount, 4] = destinataryName;
                            //xlWorksheet.Cells[rowCount, 5] = tipoCourier;
                            xlWorksheet.Cells[rowCount, 5] = CreationDatesApiList[i];
                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + ", espere un momento por favor...");
                        }
                    }
                    tracking = "";
                    order = "";
                    destinataryName = "";
                }
                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetRAGAData(JsonObjectsList[i], ShipmentsList[i]);
                    status = ChangeStatusLanguage(status);
                    if (status != "Entregado"
                        && status != "Sin descripcion por FedEx"
                        && status != "Envio cancelado por el cliente"
                        && status != "Completo por Seguimiento"
                        && Convert.ToDateTime(CreationDatesList[i]) > DateTime.Now.AddDays(-30))
                    {
                        if (tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = tracking;
                            xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i];
                            xlWorksheet.Cells[rowCount, 3].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 3] = order;
                            xlWorksheet.Cells[rowCount, 4] = destinataryName;
                            xlWorksheet.Cells[rowCount, 5] = CreationDatesApiList[i];
                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + ", espere un momento por favor...");
                        }
                    }
                    tracking = "";
                    order = "";
                    destinataryName = "";
                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios Estafeta";
                xlWorksheet.Cells[2, 1] = "Numero de guia";
                xlWorksheet.Cells[2, 2] = "Numero de control";
                xlWorksheet.Cells[2, 3] = "Numero de orden";
                xlWorksheet.Cells[2, 4] = "Nombre del cliente";
                xlWorksheet.Cells[2, 5] = "Fecha Creacion";
                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteRedpack-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                string name = "ReporteEstafeta-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email(name);
            }
            catch (SqlException e)
            {
                Console.WriteLine(" Reporte Estafeta Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        //-----------------------
        public void CreateDHLReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> CreationDatesApiList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> JsonObjectsApiList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            List<string> ShipmentsApiList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT FechaCreacion, ObjetoRequest,NumeroControl ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");

                    stringBuilder.Append("where ObjetoRequest like '%");
                    stringBuilder.Append("\"TipoCourier\":3,%'");
                    //stringBuilder.Append("AND NumeroControl LIKE 'RAGA%' ");
                    //FechaAtendido >= '2022-07-01 00:00:00.000'
                    stringBuilder.Append("AND FechaCreacion > '2022-07-01 00:00:00.000'");
                    String stringResult = stringBuilder.ToString();
                    
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesApiList.Add(reader[0].ToString());
                                JsonObjectsApiList.Add(reader[1].ToString());
                                ShipmentsApiList.Add(reader[2].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte de envios DHL";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                for (int i = 0; i < ShipmentsApiList.Count; i++)
                {
                    GetEstafetaData(JsonObjectsApiList[i]);
                    status = ChangeStatusLanguage(status);
                    if (status != "Entregado"
                        && status != "Sin descripcion por FedEx"
                        && status != "Envio cancelado por el cliente"
                        && status != "Completo por Seguimiento")
                        //&& Convert.ToDateTime(CreationDatesApiList[i]) > DateTime.Now.AddDays(-30))
                    {
                        if (tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = tracking;
                            xlWorksheet.Cells[rowCount, 2] = ShipmentsApiList[i];
                            xlWorksheet.Cells[rowCount, 3].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 3] = order;
                            xlWorksheet.Cells[rowCount, 4] = destinataryName;
                            xlWorksheet.Cells[rowCount, 5] = CreationDatesApiList[i];
                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + ", espere un momento por favor...");
                        }
                    }
                    tracking = "";
                    order = "";
                    destinataryName = "";
                }
                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetRAGAData(JsonObjectsList[i], ShipmentsList[i]);
                    status = ChangeStatusLanguage(status);
                    if (status != "Entregado"
                        && status != "Sin descripcion por FedEx"
                        && status != "Envio cancelado por el cliente"
                        && status != "Completo por Seguimiento"
                        && Convert.ToDateTime(CreationDatesList[i]) > DateTime.Now.AddDays(-30))
                    {
                        if (tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = tracking;
                            xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i];
                            xlWorksheet.Cells[rowCount, 3].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 3] = order;
                            xlWorksheet.Cells[rowCount, 4] = destinataryName;
                            xlWorksheet.Cells[rowCount, 5] = CreationDatesApiList[i];
                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + ", espere un momento por favor...");
                        }
                    }
                    tracking = "";
                    order = "";
                    destinataryName = "";
                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios DHL";
                xlWorksheet.Cells[2, 1] = "Numero de guia";
                xlWorksheet.Cells[2, 2] = "Numero de control";
                xlWorksheet.Cells[2, 3] = "Numero de orden";
                xlWorksheet.Cells[2, 4] = "Nombre del cliente";
                xlWorksheet.Cells[2, 5] = "Fecha Creacion";
                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteDHL-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                string name = "ReporteDHL-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email(name);
            }
            catch (SqlException e)
            {
                Console.WriteLine(" Reporte DHL Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }

        public void CreateRodMayReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'CORM%' ");
                    stringBuilder.Append("AND FechaCreacion > '2022-01-01'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesList.Add(reader[2].ToString());
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte de envios Rod May";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                int longitudLista = ShipmentsList.Count;
                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetApiData("Comercial Rod May SA de CV", JsonObjectsList[i], ShipmentsList[i]);

                    status = ChangeStatusLanguage(status);
                    if (shipmentType == 0)
                    {
                        if (trackingsList.Count > 0)
                        {
                            for (int j = 0; j < trackingsList.Count; j++)
                            {
                                rowCount++;
                                xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                                xlWorksheet.Cells[rowCount, 1] = trackingsList[j];
                                if (j == 0)
                                    xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i];
                                else
                                    xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i] + "-" + j;
                                xlWorksheet.Cells[rowCount, 3] = operatorName;
                                xlWorksheet.Cells[rowCount, 4] = remitentName;
                                xlWorksheet.Cells[rowCount, 5] = remitentCompany + "";
                                xlWorksheet.Cells[rowCount, 6] = destinataryName;
                                xlWorksheet.Cells[rowCount, 7] = destinataryCity;
                                xlWorksheet.Cells[rowCount, 8] = status;
                                xlWorksheet.Cells[rowCount, 9] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                                xlWorksheet.Cells[rowCount, 10] = Math.Ceiling(weight);
                                xlWorksheet.Cells[rowCount, 11] = content;

                                xlWorksheet.Cells[rowCount, 12] = heightsList[j];
                                xlWorksheet.Cells[rowCount, 13] = widthsList[j];
                                xlWorksheet.Cells[rowCount, 14] = lengthsList[j];
                                xlWorksheet.Cells[rowCount, 15] = weightsList[j];

                                Console.WriteLine("Guias procesadas = " + (rowCount - 2) + "de " + longitudLista + ", espere un momento por favor...");
                            }
                        }
                    }
                    else
                    {
                        if (tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = tracking;
                            xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i];
                            xlWorksheet.Cells[rowCount, 3] = operatorName;
                            xlWorksheet.Cells[rowCount, 4] = remitentName;
                            xlWorksheet.Cells[rowCount, 5] = remitentCompany + "";
                            xlWorksheet.Cells[rowCount, 6] = destinataryName;
                            xlWorksheet.Cells[rowCount, 7] = destinataryCity;
                            xlWorksheet.Cells[rowCount, 8] = status;
                            xlWorksheet.Cells[rowCount, 9] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                            xlWorksheet.Cells[rowCount, 10] = Math.Ceiling(weight);
                            xlWorksheet.Cells[rowCount, 11] = content;

                            xlWorksheet.Cells[rowCount, 12] = height;
                            xlWorksheet.Cells[rowCount, 13] = width;
                            xlWorksheet.Cells[rowCount, 14] = length;


                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + "de " + longitudLista + ", espere un momento por favor...");
                        }
                    }
                    content = "";
                    destinataryCity = "";
                    destinataryName = "";
                    operatorName = "";
                    remitentName = "";
                    remitentCompany = "";
                    status = "";
                    tracking = "";
                    trackingsList.Clear();

                    weight = 0;
                    weightsList.Clear();
                    width = 0;
                    widthsList.Clear();
                    length = 0;
                    lengthsList.Clear();
                    height = 0;
                    heightsList.Clear();
                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios Comercial Rod May";

                xlWorksheet.Cells[2, 1] = "Numero de guia";
                xlWorksheet.Cells[2, 2] = "Numero de control";
                xlWorksheet.Cells[2, 3] = "Operador";
                xlWorksheet.Cells[2, 4] = "Remitente";
                xlWorksheet.Cells[2, 5] = "Empresa del remitente";
                xlWorksheet.Cells[2, 6] = "Destinatario";
                xlWorksheet.Cells[2, 7] = "Ciudad destino";
                xlWorksheet.Cells[2, 8] = "Estatus del envio";
                xlWorksheet.Cells[2, 9] = "Fecha";
                xlWorksheet.Cells[2, 10] = "Peso";
                xlWorksheet.Cells[2, 11] = "Referencia";
                xlWorksheet.Cells[2, 12] = "Alto";
                xlWorksheet.Cells[2, 13] = "Ancho";
                xlWorksheet.Cells[2, 14] = "Largo";



                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteRodMay-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                string name = "ReporteRodMay-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email(name);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        public void CreateSamsoniteReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> CreationDatesApiList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> JsonObjectsApiList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            List<string> ShipmentsApiList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'SAME%' ");
                    stringBuilder.Append("AND FechaCreacion > '2021-01-01'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesList.Add(reader[2].ToString());
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'ENVI%' ");
                    stringBuilder.Append("AND FechaCreacion > '2020-02-10' ");
                    stringBuilder.Append("AND ObjetoRequest LIKE '%SAMSONITE%'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesApiList.Add(reader[2].ToString());
                                JsonObjectsApiList.Add(reader[3].ToString());
                                ShipmentsApiList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte de envios Samsonite";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                int longitudLista = ShipmentsList.Count;

                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    try {
                        GetSamsoniteData(JsonObjectsList[i], ShipmentsList[i]);

                        if (trackingsList.Count > 0)
                        {
                            for (int j = 0; j < trackingsList.Count; j++)
                            {
                                rowCount++;
                                xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                                xlWorksheet.Cells[rowCount, 1] = trackingsList[j];
                                xlWorksheet.Cells[rowCount, 2] = remitentName;
                                xlWorksheet.Cells[rowCount, 4] = destinataryName;
                                xlWorksheet.Cells[rowCount, 5] = remitentCompany + "";

                                xlWorksheet.Cells[rowCount, 6] = destinataryCity;
                                xlWorksheet.Cells[rowCount, 7] = status;
                                xlWorksheet.Cells[rowCount, 8] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                                xlWorksheet.Cells[rowCount, 9] = "";
                                try
                                {
                                    xlWorksheet.Cells[rowCount, 10] = heightsList[j];
                                    xlWorksheet.Cells[rowCount, 11] = widthsList[j];
                                    xlWorksheet.Cells[rowCount, 12] = lengthsList[j];
                                    xlWorksheet.Cells[rowCount, 13] = weightsList[j];

                                }
                                catch { }

                                Console.WriteLine("Guias procesadas = " + (rowCount - 2) + "de " + longitudLista + ", espere un momento por favor...");
                            }
                        }

                        status = ChangeStatusLanguage(status);
                        if (trackingsList.Count() == 0 && tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = tracking;

                            xlWorksheet.Cells[rowCount, 2] = remitentName;
                            //xlWorksheet.Cells[rowCount, 3] = remitentCompany;
                            xlWorksheet.Cells[rowCount, 3] = remitentNameR;
                            xlWorksheet.Cells[rowCount, 4] = destinataryName;
                            xlWorksheet.Cells[rowCount, 5] = destinaryNameC;
                            xlWorksheet.Cells[rowCount, 6] = destinataryZipCode
                                + ", " + destinataryCity
                                + ", " + destinataryState
                                + ", Mexico";
                            xlWorksheet.Cells[rowCount, 7] = status;
                            xlWorksheet.Cells[rowCount, 8] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd HH:mm:ss");
                            xlWorksheet.Cells[rowCount, 9] = "";
                            xlWorksheet.Cells[rowCount, 10] = height;
                            xlWorksheet.Cells[rowCount, 11] = width;
                            xlWorksheet.Cells[rowCount, 12] = length;
                            xlWorksheet.Cells[rowCount, 13] = weight;
                            xlWorksheet.Cells[rowCount, 14] = secured;
                            xlWorksheet.Cells[rowCount, 15] = value;

                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                        }
                        destinataryCity = "";
                        destinataryName = "";
                        destinataryState = "";
                        destinataryZipCode = "";
                        remitentName = "";
                        status = "";
                        tracking = "";
                        trackingsList.Clear();
                    }
                    catch(Exception e) { }
                }


                for (int i = 0; i < ShipmentsApiList.Count; i++)
                {
                    try {
                        GetSamsoniteApiData(JsonObjectsApiList[i]);
                        status = ChangeStatusLanguage(status);
                        if (trackingsList.Count > 0)
                        {
                            for (int j = 0; j < trackingsList.Count; j++)
                            {
                                rowCount++;
                                xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                                xlWorksheet.Cells[rowCount, 1] = trackingsList[j];
                                xlWorksheet.Cells[rowCount, 2] = remitentName;
                                //                            #if (j == 0)
                                //# xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i];
                                //#else
                                //# xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i] + "-" + j;
                                xlWorksheet.Cells[rowCount, 4] = destinataryName;
                                xlWorksheet.Cells[rowCount, 5] = remitentCompany + "";

                                xlWorksheet.Cells[rowCount, 6] = destinataryCity;
                                xlWorksheet.Cells[rowCount, 7] = status;
                                xlWorksheet.Cells[rowCount, 8] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                                xlWorksheet.Cells[rowCount, 9] = "";
                                try
                                {
                                    xlWorksheet.Cells[rowCount, 10] = heightsList[j];
                                    xlWorksheet.Cells[rowCount, 11] = widthsList[j];
                                    xlWorksheet.Cells[rowCount, 12] = lengthsList[j];
                                    xlWorksheet.Cells[rowCount, 13] = weightsList[j];

                                }
                                catch { }


                                Console.WriteLine("Guias procesadas = " + (rowCount - 2) + "de " + longitudLista + ", espere un momento por favor...");
                            }
                        }

                        if (trackingsList.Count() == 0 & tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = tracking;
                            xlWorksheet.Cells[rowCount, 2] = remitentName;
                            //xlWorksheet.Cells[rowCount, 3] = remitentCompany;
                            xlWorksheet.Cells[rowCount, 3] = remitentNameR;
                            xlWorksheet.Cells[rowCount, 4] = destinataryName;
                            xlWorksheet.Cells[rowCount, 5] = destinaryNameC;

                            xlWorksheet.Cells[rowCount, 6] = destinataryZipCode
                                + ", " + destinataryCity
                                + ", " + destinataryState
                                + ", Mexico";
                            xlWorksheet.Cells[rowCount, 7] = status;
                            xlWorksheet.Cells[rowCount, 8] = Convert.ToDateTime(CreationDatesApiList[i]).ToString("yyyy'-'MM'-'dd HH:mm:ss");
                            xlWorksheet.Cells[rowCount, 9] = "";
                            xlWorksheet.Cells[rowCount, 10] = height;
                            xlWorksheet.Cells[rowCount, 11] = width;
                            xlWorksheet.Cells[rowCount, 12] = length;
                            xlWorksheet.Cells[rowCount, 13] = weight;
                            xlWorksheet.Cells[rowCount, 14] = secured;
                            xlWorksheet.Cells[rowCount, 15] = value;

                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                        }
                        destinataryCity = "";
                        destinataryName = "";
                        destinataryState = "";
                        destinataryZipCode = "";
                        remitentName = "";
                        remitentCompany = "";
                        status = "";
                        tracking = "";

                        weight = 0;
                        weightsList.Clear();
                        width = 0;
                        widthsList.Clear();
                        length = 0;
                        lengthsList.Clear();
                        height = 0;
                        heightsList.Clear();
                        trackingsList.Clear();

                    }
                    catch(Exception e) {
                    }
                    

                }



                xlWorksheet.Cells[1, 1] = "Reporte de envios Samsonite";
                xlWorksheet.Cells[2, 1] = "Numero de guia";
                xlWorksheet.Cells[2, 2] = "Nombre del remitente";
                xlWorksheet.Cells[2, 3] = "Empresa del remitente";

                xlWorksheet.Cells[2, 4] = "Nombre del destinatario";
                xlWorksheet.Cells[2, 5] = "Empresa del destinatario";


                xlWorksheet.Cells[2, 6] = "Direccion del destinatario";

                xlWorksheet.Cells[2, 7] = "Estatus";
                xlWorksheet.Cells[2, 8] = "Fecha del envio";
                xlWorksheet.Cells[2, 9] = "Comentarios";

                xlWorksheet.Cells[2, 10] = "Alto";
                xlWorksheet.Cells[2, 11] = "Ancho";
                xlWorksheet.Cells[2, 12] = "Largo";
                xlWorksheet.Cells[2, 13] = "Peso";
                xlWorksheet.Cells[2, 14] = "Asegurado";
                xlWorksheet.Cells[2, 15] = "Valor";


                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteSamsonite-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                string name = "ReporteSamsonite-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email(name);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        public void CreateSamsoniteRetornosReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> CreationDatesApiList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> JsonObjectsApiList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            List<string> ShipmentsApiList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'SARE%' ");
                    stringBuilder.Append("AND FechaCreacion > '2021-02-10'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesList.Add(reader[2].ToString());
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }

                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte Samsonite Retornos";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetSamsoniteRetornosData(JsonObjectsList[i], ShipmentsList[i]);
                    for (int j = 0; j < statusList.Count; j++)
                        statusList[j] = ChangeStatusLanguage(statusList[j]);
                    if (trackingsList.Count != 0)
                    {
                        for (int j = 0; j < trackingsList.Count; j++)
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = trackingsList[j];
                            if (j == 0)
                                xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i];
                            else
                                xlWorksheet.Cells[rowCount, 2] = ShipmentsList[i] + "-" + j;
                            xlWorksheet.Cells[rowCount, 3] = remitentName;
                            xlWorksheet.Cells[rowCount, 4] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd HH:mm:ss");
                            if (trackingsList.Count == statusList.Count)
                                xlWorksheet.Cells[rowCount, 5] = statusList[j];
                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + ", espere un momento por favor...");
                        }
                    }
                    destinataryCity = "";
                    destinataryName = "";
                    destinataryState = "";
                    destinataryZipCode = "";
                    remitentName = "";
                    statusList.Clear();
                    trackingsList.Clear();
                }
                xlWorksheet.Cells[1, 1] = "Reporte Samsonite Retornos";
                xlWorksheet.Cells[2, 1] = "Numero de guia";
                xlWorksheet.Cells[2, 2] = "Numero de control";
                xlWorksheet.Cells[2, 3] = "Nombre del remitente";
                xlWorksheet.Cells[2, 4] = "Fecha de envio";
                xlWorksheet.Cells[2, 5] = "Estatus";
                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteSamsoniteRetornos-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                string name = "ReporteSamsoniteRetornos-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email(name);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        public void CreateSotanoReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'LISO%' ");
                    stringBuilder.Append("AND FechaCreacion > '2021-03-15'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesList.Add(reader[2].ToString());
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte de Libreria El Sotano";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetSotanoData(JsonObjectsList[i], ShipmentsList[i]);
                    status = ChangeStatusLanguage(status);
                    if (tracking != "")
                    {
                        rowCount++;
                        xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i];
                        xlWorksheet.Cells[rowCount, 2] = remitentZipCode
                            + ", " + remitentCity
                            + ", " + remitentState
                            + ", Mexico";
                        xlWorksheet.Cells[rowCount, 3] = destinataryZipCode
                            + ", " + destinataryCity
                            + ", " + destinataryState
                            + ", Mexico";
                        xlWorksheet.Cells[rowCount, 4] = serviceName;
                        xlWorksheet.Cells[rowCount, 5].NumberFormat = "@";
                        xlWorksheet.Cells[rowCount, 5] = tracking;
                        xlWorksheet.Cells[rowCount, 6] = status;
                        xlWorksheet.Cells[rowCount, 7] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd HH:mm:ss");
                        Console.WriteLine("Guias procesadas = " + (rowCount - 2) + ", espere un momento por favor...");
                    }
                    destinataryCity = "";
                    destinataryState = "";
                    destinataryZipCode = "";
                    remitentCity = "";
                    remitentState = "";
                    remitentZipCode = "";
                    serviceName = "";
                    status = "";
                    tracking = "";
                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios Libreria El Sotano";
                xlWorksheet.Cells[2, 1] = "Numero de control";
                xlWorksheet.Cells[2, 2] = "Direccion del remitente";
                xlWorksheet.Cells[2, 3] = "Direccion del destinatario";
                xlWorksheet.Cells[2, 4] = "Tipo de servicio";
                xlWorksheet.Cells[2, 5] = "Numero de guia";
                xlWorksheet.Cells[2, 6] = "Estatus";
                xlWorksheet.Cells[2, 7] = "Fecha de envio";
                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteSotano-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                string name = "ReporteSotano-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email(name);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        public void CreateUffiReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'UFFI%' ");
                    stringBuilder.Append("AND FechaCreacion > '2021-01-01'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesList.Add(reader[2].ToString());
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte de envios Uffi Dreams";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                int longitudLista = ShipmentsList.Count;

                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetApiData("UFFI DREAMS", JsonObjectsList[i], ShipmentsList[i]);
                    status = ChangeStatusLanguage(status);
                    if (shipmentType == 0)
                    {
                        if (trackingsList.Count > 0)
                        {
                            for (int j = 0; j < trackingsList.Count; j++)
                            {
                                rowCount++;
                                xlWorksheet.Cells[rowCount, 1] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                                xlWorksheet.Cells[rowCount, 2].NumberFormat = "@";
                                xlWorksheet.Cells[rowCount, 2] = trackingsList[j];
                                if (j == 0)
                                    xlWorksheet.Cells[rowCount, 3] = ShipmentsList[i];
                                else
                                    xlWorksheet.Cells[rowCount, 3] = ShipmentsList[i] + "-" + j;
                                xlWorksheet.Cells[rowCount, 4] = operatorName;
                                xlWorksheet.Cells[rowCount, 5] = remitentName;
                                xlWorksheet.Cells[rowCount, 6] = remitentCompany;
                                xlWorksheet.Cells[rowCount, 7] = destinataryName;
                                xlWorksheet.Cells[rowCount, 8] = destinataryCity;
                                xlWorksheet.Cells[rowCount, 9] = status;
                                xlWorksheet.Cells[rowCount, 10] = heightsList[j];
                                xlWorksheet.Cells[rowCount, 11] = widthsList[j];
                                xlWorksheet.Cells[rowCount, 12] = lengthsList[j];
                                xlWorksheet.Cells[rowCount, 13] = weightsList[j];

                                Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                            }
                        }
                    }
                    else
                    {
                        if (tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                            xlWorksheet.Cells[rowCount, 2].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 2] = tracking;
                            xlWorksheet.Cells[rowCount, 3] = ShipmentsList[i];
                            xlWorksheet.Cells[rowCount, 4] = operatorName;
                            xlWorksheet.Cells[rowCount, 5] = remitentName;
                            xlWorksheet.Cells[rowCount, 6] = remitentCompany;
                            xlWorksheet.Cells[rowCount, 7] = destinataryName;
                            xlWorksheet.Cells[rowCount, 8] = destinataryCity;
                            xlWorksheet.Cells[rowCount, 9] = status;
                            xlWorksheet.Cells[rowCount, 10] = height;
                            xlWorksheet.Cells[rowCount, 11] = width;
                            xlWorksheet.Cells[rowCount, 12] = length;
                            xlWorksheet.Cells[rowCount, 13] = weight;

                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                        }
                    }
                    destinataryCity = "";
                    destinataryName = "";
                    operatorName = "";
                    remitentName = "";
                    remitentCompany = "";
                    status = "";
                    tracking = "";
                    trackingsList.Clear();

                    weight = 0;
                    weightsList.Clear();
                    width = 0;
                    widthsList.Clear();
                    length = 0;
                    lengthsList.Clear();
                    height = 0;
                    heightsList.Clear();
                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios Uffi Dreams";
                xlWorksheet.Cells[2, 1] = "Fecha";
                xlWorksheet.Cells[2, 2] = "Numero de guia";
                xlWorksheet.Cells[2, 3] = "Numero de control";
                xlWorksheet.Cells[2, 4] = "Operador";
                xlWorksheet.Cells[2, 5] = "Remitente";
                xlWorksheet.Cells[2, 6] = "Empresa del remitente";
                xlWorksheet.Cells[2, 7] = "Destinatario";
                xlWorksheet.Cells[2, 8] = "Ciudad destino";
                xlWorksheet.Cells[2, 9] = "Estatus del envio";
                xlWorksheet.Cells[2, 10] = "Alto";
                xlWorksheet.Cells[2, 11] = "Ancho";
                xlWorksheet.Cells[2, 12] = "Largo";
                xlWorksheet.Cells[2, 13] = "Peso";
                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteUffi-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                string name = "ReporteUffi-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email(name);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        public void CreateBerdicoReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'BERD%' ");
                    stringBuilder.Append("AND FechaCreacion > '2021-01-01'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesList.Add(reader[2].ToString());
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte de envios Berdico";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                int longitudLista = ShipmentsList.Count;

                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetApiData("Berdico", JsonObjectsList[i], ShipmentsList[i]);
                    status = ChangeStatusLanguage(status);
                    if (shipmentType == 0)
                    {
                        if (trackingsList.Count > 0)
                        {
                            for (int j = 0; j < trackingsList.Count; j++)
                            {
                                rowCount++;
                                xlWorksheet.Cells[rowCount, 1] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                                xlWorksheet.Cells[rowCount, 2].NumberFormat = "@";
                                xlWorksheet.Cells[rowCount, 2] = trackingsList[j];
                                xlWorksheet.Cells[rowCount, 3] = operatorName;
                                xlWorksheet.Cells[rowCount, 4] = remitentName;
                                xlWorksheet.Cells[rowCount, 5] = remitentCompany;
                                xlWorksheet.Cells[rowCount, 6] = destinataryName;
                                xlWorksheet.Cells[rowCount, 7] = destinataryCity;
                                xlWorksheet.Cells[rowCount, 8] = status;
                                xlWorksheet.Cells[rowCount, 9] = heightsList[j];
                                xlWorksheet.Cells[rowCount, 10] = widthsList[j];
                                xlWorksheet.Cells[rowCount, 11] = lengthsList[j];
                                xlWorksheet.Cells[rowCount, 12] = weightsList[j];


                                Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                            }
                        }
                    }
                    else
                    {
                        if (tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                            xlWorksheet.Cells[rowCount, 2].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 2] = tracking;
                            xlWorksheet.Cells[rowCount, 3] = operatorName;
                            xlWorksheet.Cells[rowCount, 4] = remitentName;
                            xlWorksheet.Cells[rowCount, 5] = remitentCompany;
                            xlWorksheet.Cells[rowCount, 6] = destinataryName;
                            xlWorksheet.Cells[rowCount, 7] = destinataryCity;
                            xlWorksheet.Cells[rowCount, 8] = status;
                            xlWorksheet.Cells[rowCount, 9] = height;
                            xlWorksheet.Cells[rowCount, 10] = width;
                            xlWorksheet.Cells[rowCount, 11] = length;
                            xlWorksheet.Cells[rowCount, 12] = weight;

                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                        }
                    }
                    destinataryCity = "";
                    destinataryName = "";
                    operatorName = "";
                    remitentName = "";
                    remitentCompany = "";
                    status = "";
                    tracking = "";
                    trackingsList.Clear();

                    weight = 0;
                    weightsList.Clear();
                    width = 0;
                    widthsList.Clear();
                    length = 0;
                    lengthsList.Clear();
                    height = 0;
                    heightsList.Clear();

                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios Berdico";
                xlWorksheet.Cells[2, 1] = "Fecha";
                xlWorksheet.Cells[2, 2] = "Numero de guia";
                xlWorksheet.Cells[2, 3] = "Operador";
                xlWorksheet.Cells[2, 4] = "Remitente";
                xlWorksheet.Cells[2, 5] = "Empresa del remitente";
                xlWorksheet.Cells[2, 5] = "Destinatario";
                xlWorksheet.Cells[2, 6] = "Nombre destino";
                xlWorksheet.Cells[2, 7] = "Cuidad Destino";
                xlWorksheet.Cells[2, 8] = "Estatus del envio";
                xlWorksheet.Cells[2, 9] = "Alto";
                xlWorksheet.Cells[2, 10] = "Ancho";
                xlWorksheet.Cells[2, 11] = "Largo";
                xlWorksheet.Cells[2, 12] = "Peso";

                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteBerdico-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                string name = "ReporteBerdico-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email(name);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        public void CreateVCVReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> CreationApiDatesList = new List<string>();
            List<string> JsonObjectsApiList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'ENVI%' ");
                    stringBuilder.Append("AND FechaCreacion > '2021-01-01' ");
                    stringBuilder.Append("AND ObjetoRequest LIKE '%VCV PUBLICACIONES%'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationApiDatesList.Add(reader[2].ToString());
                                JsonObjectsApiList.Add(reader[3].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'VCVP%' ");
                    stringBuilder.Append("AND FechaCreacion > '2021-01-01' ");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesList.Add(reader[2].ToString());
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte de envios VCV";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                int longitudLista = ShipmentsList.Count;

                for (int i = 0; i < JsonObjectsApiList.Count; i++)
                {
                    try {
                        GetVCVApiData(JsonObjectsApiList[i]);
                        if (tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = tracking;
                            xlWorksheet.Cells[rowCount, 2] = destinataryZipCode
                                + ", " + destinataryCity
                                + ", " + destinataryState
                                + ", " + destinataryCountry;
                            xlWorksheet.Cells[rowCount, 3] = destinataryCompany;
                            xlWorksheet.Cells[rowCount, 4] = destinataryName;
                            xlWorksheet.Cells[rowCount, 5] = remitentName;
                            xlWorksheet.Cells[rowCount, 6] = remitentCompany;
                            xlWorksheet.Cells[rowCount, 7] = operatorName;
                            xlWorksheet.Cells[rowCount, 8] = Convert.ToDateTime(CreationApiDatesList[i]).ToString("yyyy'-'MM'-'dd");
                            xlWorksheet.Cells[rowCount, 9] = height;
                            xlWorksheet.Cells[rowCount, 10] = width;
                            xlWorksheet.Cells[rowCount, 11] = length;
                            xlWorksheet.Cells[rowCount, 12] = weight;

                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                        }
                        destinataryCity = "";
                        destinataryCompany = "";
                        destinataryCountry = "";
                        destinataryName = "";
                        destinataryState = "";
                        destinataryZipCode = "";
                        remitentName = "";
                        remitentCompany = "";
                        operatorName = "";
                        tracking = "";
                    }
                    catch(Exception e) { }
                    
                }
                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    try {
                        GetApiData("VCV Publicaciones", JsonObjectsList[i], ShipmentsList[i]);
                        status = ChangeStatusLanguage(status);
                        if (shipmentType == 0)
                        {
                            if (trackingsList.Count > 0)
                            {
                                for (int j = 0; j < trackingsList.Count; j++)
                                {
                                    rowCount++;
                                    xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                                    xlWorksheet.Cells[rowCount, 1] = trackingsList[j];
                                    xlWorksheet.Cells[rowCount, 2] = destinataryZipCode
                                        + ", " + destinataryCity
                                        + ", " + destinataryState
                                        + ", " + destinataryCountry;
                                    xlWorksheet.Cells[rowCount, 3] = destinataryCompany;
                                    xlWorksheet.Cells[rowCount, 4] = destinataryName;
                                    xlWorksheet.Cells[rowCount, 5] = remitentName;
                                    xlWorksheet.Cells[rowCount, 6] = remitentCompany;
                                    xlWorksheet.Cells[rowCount, 7] = operatorName;
                                    xlWorksheet.Cells[rowCount, 8] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                                    xlWorksheet.Cells[rowCount, 9] = heightsList[j];
                                    xlWorksheet.Cells[rowCount, 10] = widthsList[j];
                                    xlWorksheet.Cells[rowCount, 11] = lengthsList[j];
                                    xlWorksheet.Cells[rowCount, 12] = weightsList[j];



                                    Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                                }
                            }
                        }
                        else
                        {
                            if (tracking != "")
                            {
                                rowCount++;
                                xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                                xlWorksheet.Cells[rowCount, 1] = tracking;
                                xlWorksheet.Cells[rowCount, 2] = destinataryZipCode
                                    + ", " + destinataryCity
                                    + ", " + destinataryState
                                    + ", " + destinataryCountry;
                                xlWorksheet.Cells[rowCount, 3] = destinataryCompany;
                                xlWorksheet.Cells[rowCount, 4] = destinataryName;
                                xlWorksheet.Cells[rowCount, 5] = remitentName;
                                xlWorksheet.Cells[rowCount, 6] = remitentCompany;
                                xlWorksheet.Cells[rowCount, 7] = operatorName;
                                xlWorksheet.Cells[rowCount, 8] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");

                                xlWorksheet.Cells[rowCount, 9] = height;
                                xlWorksheet.Cells[rowCount, 10] = width;
                                xlWorksheet.Cells[rowCount, 11] = length;
                                xlWorksheet.Cells[rowCount, 12] = weight;

                                Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                            }
                        }
                        destinataryCity = "";
                        destinataryCompany = "";
                        destinataryCountry = "";
                        destinataryName = "";
                        destinataryState = "";
                        destinataryZipCode = "";
                        remitentName = "";
                        remitentCompany = "";
                        operatorName = "";
                        tracking = "";
                        trackingsList.Clear();

                        weight = 0;
                        weightsList.Clear();
                        width = 0;
                        widthsList.Clear();
                        length = 0;
                        lengthsList.Clear();
                        height = 0;
                        heightsList.Clear();
                    }
                    catch(Exception e) { }


                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios VCV";
                xlWorksheet.Cells[2, 1] = "Numero de guia";
                xlWorksheet.Cells[2, 2] = "Direccion del destinatario";
                xlWorksheet.Cells[2, 3] = "Empresa del destinatario";
                xlWorksheet.Cells[2, 4] = "Nombre del destinatario";
                xlWorksheet.Cells[2, 5] = "Nombre del remitente";
                xlWorksheet.Cells[2, 5] = "Empresa del remitente";
                xlWorksheet.Cells[2, 6] = "Operador";
                xlWorksheet.Cells[2, 7] = "Fecha de envio";
                xlWorksheet.Cells[2, 8] = "Estatus";
                xlWorksheet.Cells[2, 9] = "Alto";
                xlWorksheet.Cells[2, 10] = "Ancho";
                xlWorksheet.Cells[2, 11] = "Largo";
                xlWorksheet.Cells[2, 12] = "Peso";

                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteVCV-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                string name = "ReporteVCV-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email(name);


            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }
        public void CreateIpisaReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'INPI%' ");
                    stringBuilder.Append("AND FechaCreacion > '2021-01-01'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesList.Add(reader[2].ToString());
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte IPISA";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                int longitudLista = ShipmentsList.Count;

                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetApiData("Instrumentos y Productos Industriales SA de CV", JsonObjectsList[i], ShipmentsList[i]);
                    status = ChangeStatusLanguage(status);
                    if (shipmentType == 0)
                    {
                        if (trackingsList.Count > 0)
                        {
                            for (int j = 0; j < trackingsList.Count; j++)
                            {
                                rowCount++;
                                xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                                if (j == 0)
                                    xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i];
                                else
                                    xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i] + "-" + j;
                                xlWorksheet.Cells[rowCount, 2].NumberFormat = "@";
                                xlWorksheet.Cells[rowCount, 2] = trackingsList[j];
                                xlWorksheet.Cells[rowCount, 3] = operatorName;
                                xlWorksheet.Cells[rowCount, 4] = remitentName;
                                xlWorksheet.Cells[rowCount, 5] = remitentCompany;
                                xlWorksheet.Cells[rowCount, 6] = destinataryName;
                                xlWorksheet.Cells[rowCount, 7] = status;

                                xlWorksheet.Cells[rowCount, 8] = heightsList[j];
                                xlWorksheet.Cells[rowCount, 9] = widthsList[j];
                                xlWorksheet.Cells[rowCount, 10] = lengthsList[j];
                                xlWorksheet.Cells[rowCount, 11] = weightsList[j];


                                Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                            }
                        }
                    }
                    else
                    {
                        if (tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i];
                            xlWorksheet.Cells[rowCount, 2].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 2] = tracking;
                            xlWorksheet.Cells[rowCount, 3] = operatorName;
                            xlWorksheet.Cells[rowCount, 4] = remitentName;
                            xlWorksheet.Cells[rowCount, 5] = remitentCompany;
                            xlWorksheet.Cells[rowCount, 6] = destinataryName;
                            xlWorksheet.Cells[rowCount, 7] = status;

                            xlWorksheet.Cells[rowCount, 8] = height;
                            xlWorksheet.Cells[rowCount, 9] = width;
                            xlWorksheet.Cells[rowCount, 10] = length;
                            xlWorksheet.Cells[rowCount, 11] = weight;


                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                        }
                    }
                    destinataryCity = "";
                    destinataryCountry = "";
                    destinataryState = "";
                    destinataryZipCode = "";
                    operatorName = "";
                    remitentCity = "";
                    remitentCompany = "";
                    remitentCountry = "";
                    remitentState = "";
                    remitentZipCode = "";
                    serviceName = "";
                    status = "";
                    tracking = "";
                    trackingsList.Clear();

                    weight = 0;
                    weightsList.Clear();
                    width = 0;
                    widthsList.Clear();
                    length = 0;
                    lengthsList.Clear();
                    height = 0;
                    heightsList.Clear();

                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios Instrumentos y Productos Industriales SA de CV";
                xlWorksheet.Cells[2, 1] = "No. Solicitud"; //1
                xlWorksheet.Cells[2, 2] = "Guia";//2
                xlWorksheet.Cells[2, 3] = "Operador"; //3
                xlWorksheet.Cells[2, 4] = "Remitente";//4
                xlWorksheet.Cells[2, 5] = "Empresa del remitente";//5
                xlWorksheet.Cells[2, 6] = "Destinatario";//6
                xlWorksheet.Cells[2, 9] = "Estatus del envio";//7

                xlWorksheet.Cells[2, 10] = "Alto";
                xlWorksheet.Cells[2, 11] = "Ancho";
                xlWorksheet.Cells[2, 12] = "Largo";
                xlWorksheet.Cells[2, 13] = "Peso";

                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteIPISA-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                string name = "ReporteIPISA-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email(name);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }

        public void CreateSoniaALReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'SOAL%' ");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesList.Add(reader[2].ToString());
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte SONIA ALEJANDRA BRAUER";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetApiData("SONIA ALEJANDRA BRAUER VILLASEÑOR", JsonObjectsList[i], ShipmentsList[i]);
                    status = ChangeStatusLanguage(status);
                    if (shipmentType == 0)
                    {
                        if (trackingsList.Count > 0)
                        {
                            for (int j = 0; j < trackingsList.Count; j++)
                            {
                                rowCount++;
                                xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                                if (j == 0)
                                    xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i];
                                else
                                    xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i] + "-" + j;
                                xlWorksheet.Cells[rowCount, 2].NumberFormat = "@";
                                xlWorksheet.Cells[rowCount, 2] = trackingsList[j];
                                xlWorksheet.Cells[rowCount, 3] = operatorName;

                                xlWorksheet.Cells[rowCount, 4] = remitentName;
                                xlWorksheet.Cells[rowCount, 5] = remitentCompany;
                                //////Remitente
                                //xlWorksheet.Cells[rowCount, 14] = remitentContactName;
                                xlWorksheet.Cells[rowCount, 6] = remitentPhonenumber;
                                xlWorksheet.Cells[rowCount, 7] = remitentEmail;
                                xlWorksheet.Cells[rowCount, 8] = remitentCountry;
                                xlWorksheet.Cells[rowCount, 9] = remitentState;
                                xlWorksheet.Cells[rowCount, 10] = remitentCity;
                                xlWorksheet.Cells[rowCount, 11] = remitentBlock;
                                xlWorksheet.Cells[rowCount, 12] = remitentStreet;
                                xlWorksheet.Cells[rowCount, 13] = remitentNumber;
                                xlWorksheet.Cells[rowCount, 14] = remitentZipCode;
                                //Destino
                                xlWorksheet.Cells[rowCount, 15] = destinataryName;
                                xlWorksheet.Cells[rowCount, 16] = destinataryCompany;
                                xlWorksheet.Cells[rowCount, 17] = destinaryPhonenumber;
                                xlWorksheet.Cells[rowCount, 18] = destinaryEmail;
                                xlWorksheet.Cells[rowCount, 19] = destinataryCountry;
                                xlWorksheet.Cells[rowCount, 22] = destinataryState;
                                xlWorksheet.Cells[rowCount, 21] = destinataryCity;
                                xlWorksheet.Cells[rowCount, 22] = destinaryBlock;
                                xlWorksheet.Cells[rowCount, 23] = destinaryStreet;
                                xlWorksheet.Cells[rowCount, 24] = destinaryNumber;
                                xlWorksheet.Cells[rowCount, 25] = destinataryZipCode;


                                xlWorksheet.Cells[rowCount, 26] = status;
                                xlWorksheet.Cells[rowCount, 27] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");

                                Console.WriteLine("Guias procesadas = " + (rowCount - 2) + ", espere un momento por favor...");
                            }
                        }
                    }
                    else
                    {
                        if (tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i];
                            xlWorksheet.Cells[rowCount, 2].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 2] = tracking;
                            xlWorksheet.Cells[rowCount, 3] = operatorName;

                            xlWorksheet.Cells[rowCount, 4] = remitentName;
                            xlWorksheet.Cells[rowCount, 5] = remitentCompany;
                            //////Remitente
                            //xlWorksheet.Cells[rowCount, 14] = remitentContactName;
                            xlWorksheet.Cells[rowCount, 6] = remitentPhonenumber;
                            xlWorksheet.Cells[rowCount, 7] = remitentEmail;
                            xlWorksheet.Cells[rowCount, 8] = remitentCountry;
                            xlWorksheet.Cells[rowCount, 9] = remitentState;
                            xlWorksheet.Cells[rowCount, 10] = remitentCity;
                            xlWorksheet.Cells[rowCount, 11] = remitentBlock;
                            xlWorksheet.Cells[rowCount, 12] = remitentStreet;
                            xlWorksheet.Cells[rowCount, 13] = remitentNumber;
                            xlWorksheet.Cells[rowCount, 14] = remitentZipCode;
                            //Destino
                            xlWorksheet.Cells[rowCount, 15] = destinataryName;
                            xlWorksheet.Cells[rowCount, 16] = destinataryCompany;
                            xlWorksheet.Cells[rowCount, 17] = destinaryPhonenumber;
                            xlWorksheet.Cells[rowCount, 18] = destinaryEmail;
                            xlWorksheet.Cells[rowCount, 19] = destinataryCountry;
                            xlWorksheet.Cells[rowCount, 20] = destinataryState;
                            xlWorksheet.Cells[rowCount, 21] = destinataryCity;
                            xlWorksheet.Cells[rowCount, 22] = destinaryBlock;
                            xlWorksheet.Cells[rowCount, 23] = destinaryStreet;
                            xlWorksheet.Cells[rowCount, 24] = destinaryNumber;
                            xlWorksheet.Cells[rowCount, 25] = destinataryZipCode;


                            xlWorksheet.Cells[rowCount, 26] = status;
                            xlWorksheet.Cells[rowCount, 27] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");


                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + ", espere un momento por favor...");
                        }
                    }
                    destinataryCity = "";
                    destinataryCountry = "";
                    destinataryState = "";
                    destinataryZipCode = "";
                    operatorName = "";
                    remitentCity = "";
                    remitentCompany = "";
                    remitentCountry = "";
                    remitentState = "";
                    remitentZipCode = "";
                    serviceName = "";
                    status = "";
                    tracking = "";
                    trackingsList.Clear();
                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios SONIA ALEJANDRA BRAUER VILLASEÑOR";
                xlWorksheet.Cells[2, 1] = "No. Solicitud"; //1
                xlWorksheet.Cells[2, 2] = "Guia";//2
                xlWorksheet.Cells[2, 3] = "Operador"; //3

                xlWorksheet.Cells[2, 4] = "Remitente";//4
                xlWorksheet.Cells[2, 5] = "Empresa del remitente";//5
                xlWorksheet.Cells[2, 6] = "Telefono del remitente";//5f
                xlWorksheet.Cells[2, 7] = "Email del remitente";//5
                xlWorksheet.Cells[2, 8] = "Pais del remitente";//5
                xlWorksheet.Cells[2, 9] = "Estado del remitente";//5
                xlWorksheet.Cells[2, 10] = "Ciudad del remitente";//5
                xlWorksheet.Cells[2, 11] = "Colonia del remitente";//5
                xlWorksheet.Cells[2, 12] = "Calle del remitente";//5
                xlWorksheet.Cells[2, 13] = "Numero del remitente";//5
                xlWorksheet.Cells[2, 14] = "C.P. del remitente";//5

                xlWorksheet.Cells[2, 15] = "Destinatario";//6
                xlWorksheet.Cells[2, 16] = "Empresa del destinatario";//5
                xlWorksheet.Cells[2, 17] = "Telefono del destinatario";//5
                xlWorksheet.Cells[2, 18] = "Email del destinatario";//5
                xlWorksheet.Cells[2, 19] = "Pais del destinatario";//5
                xlWorksheet.Cells[2, 20] = "Estado del destinatario";//5f
                xlWorksheet.Cells[2, 21] = "Ciudad del destinatario";//5
                xlWorksheet.Cells[2, 22] = "Colonia del destinatario";//5
                xlWorksheet.Cells[2, 23] = "Calle del destinatario";//
                xlWorksheet.Cells[2, 24] = "Numero del destinatario";//
                xlWorksheet.Cells[2, 25] = "C.P. del destinatario";//

                xlWorksheet.Cells[2, 26] = "Estatus del envio";//7
                xlWorksheet.Cells[2, 27] = "Fecha de envio";

                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("Reporte_SONIA_ALEJANDRA_BRAUER-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                string name = "Reporte_SONIA_ALEJANDRA_BRAUER-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email(name);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }
        }


        //public void CreateSoniaALReport()
        //{
        //    List<string> CreationDatesList = new List<string>();
        //    List<string> JsonObjectsList = new List<string>();
        //    List<string> ShipmentsList = new List<string>();
        //    try
        //    {
        //        SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
        //        {
        //            DataSource = "Traps-carmar.Database.windows.net",
        //            UserID = "Thrimex",
        //            Password = "Caradmin2018!",
        //            InitialCatalog = "TRAPS-ADMIN"
        //        };
        //        using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
        //        {
        //            connection.Open();
        //            StringBuilder stringBuilder = new StringBuilder();
        //            stringBuilder.Append("SELECT * ");
        //            stringBuilder.Append("FROM PeticionApiEnvios ");
        //            stringBuilder.Append("WHERE Status = 1 ");
        //            stringBuilder.Append("AND NumeroControl LIKE 'SOAL%' ");
        //            String stringResult = stringBuilder.ToString();
        //            using (SqlCommand command = new SqlCommand(stringResult, connection))
        //            {
        //                using (SqlDataReader reader = command.ExecuteReader())
        //                {
        //                    while (reader.Read())
        //                    {
        //                        CreationDatesList.Add(reader[2].ToString());
        //                        JsonObjectsList.Add(reader[3].ToString());
        //                        ShipmentsList.Add(reader[6].ToString());
        //                    }
        //                }
        //            }
        //            connection.Close();
        //        }
        //        Application xlApp = new Application();
        //        Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
        //        Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
        //        xlApp.Visible = false;
        //        xlApp.DisplayAlerts = false;
        //        xlWorksheet.Name = "Reporte SONIA ALEJANDRA BRAUER";
        //        xlWorksheet.Cells.Font.Size = 12;
        //        int rowCount = 2;
        //        for (int i = 0; i < ShipmentsList.Count; i++)
        //        {
        //            GetApiData("SONIA ALEJANDRA BRAUER VILLASEÑOR", JsonObjectsList[i], ShipmentsList[i]);
        //            status = ChangeStatusLanguage(status);
        //            if (shipmentType == 0)
        //            {
        //                if (trackingsList.Count > 0)
        //                {
        //                    for (int j = 0; j < trackingsList.Count; j++)
        //                    {
        //                        rowCount++;
        //                        xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
        //                        if (j == 0)
        //                            xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i];
        //                        else
        //                            xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i] + "-" + j;
        //                        xlWorksheet.Cells[rowCount, 2].NumberFormat = "@";
        //                        xlWorksheet.Cells[rowCount, 2] = trackingsList[j];
        //                        xlWorksheet.Cells[rowCount, 3] = operatorName;

        //                        xlWorksheet.Cells[rowCount, 4] = remitentName;
        //                        xlWorksheet.Cells[rowCount, 5] = remitentCompany;
        //                        //////Remitente
        //                        //xlWorksheet.Cells[rowCount, 14] = remitentContactName;
        //                        xlWorksheet.Cells[rowCount, 6] = remitentPhonenumber;
        //                        xlWorksheet.Cells[rowCount, 7] = remitentEmail;
        //                        xlWorksheet.Cells[rowCount, 8] = remitentCountry;
        //                        xlWorksheet.Cells[rowCount, 9] = remitentState;
        //                        xlWorksheet.Cells[rowCount, 10] = remitentCity;
        //                        xlWorksheet.Cells[rowCount, 11] = remitentBlock;
        //                        xlWorksheet.Cells[rowCount, 12] = remitentStreet;
        //                        xlWorksheet.Cells[rowCount, 13] = remitentNumber;
        //                        xlWorksheet.Cells[rowCount, 14] = remitentZipCode;
        //                        //Destino
        //                        xlWorksheet.Cells[rowCount, 15] = destinataryName;
        //                        xlWorksheet.Cells[rowCount, 16] = destinataryCompany;
        //                        xlWorksheet.Cells[rowCount, 17] = destinaryPhonenumber;
        //                        xlWorksheet.Cells[rowCount, 18] = destinaryEmail;
        //                        xlWorksheet.Cells[rowCount, 19] = remitentCountry;
        //                        xlWorksheet.Cells[rowCount, 22] = remitentState;
        //                        xlWorksheet.Cells[rowCount, 21] = destinataryCity;
        //                        xlWorksheet.Cells[rowCount, 22] = destinaryBlock;
        //                        xlWorksheet.Cells[rowCount, 23] = destinaryStreet;
        //                        xlWorksheet.Cells[rowCount, 24] = destinaryNumber;
        //                        xlWorksheet.Cells[rowCount, 25] = destinataryZipCode;


        //                        xlWorksheet.Cells[rowCount, 26] = status;

        //                        Console.WriteLine("Guias procesadas = " + (rowCount - 2) + ", espere un momento por favor...");
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                if (tracking != "")
        //                {
        //                    rowCount++;
        //                    xlWorksheet.Cells[rowCount, 1].NumberFormat = "@";
        //                    xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i];
        //                    xlWorksheet.Cells[rowCount, 2].NumberFormat = "@";
        //                    xlWorksheet.Cells[rowCount, 2] = tracking;
        //                    xlWorksheet.Cells[rowCount, 3] = operatorName;

        //                    xlWorksheet.Cells[rowCount, 4] = remitentName;
        //                    xlWorksheet.Cells[rowCount, 5] = remitentCompany;
        //                    //////Remitente
        //                    //xlWorksheet.Cells[rowCount, 14] = remitentContactName;
        //                    xlWorksheet.Cells[rowCount, 6] = remitentPhonenumber;
        //                    xlWorksheet.Cells[rowCount, 7] = remitentEmail;
        //                    xlWorksheet.Cells[rowCount, 8] = remitentCountry;
        //                    xlWorksheet.Cells[rowCount, 9] = remitentState;
        //                    xlWorksheet.Cells[rowCount, 10] = remitentCity;
        //                    xlWorksheet.Cells[rowCount, 11] = remitentBlock;
        //                    xlWorksheet.Cells[rowCount, 12] = remitentStreet;
        //                    xlWorksheet.Cells[rowCount, 13] = remitentNumber;
        //                    xlWorksheet.Cells[rowCount, 14] = remitentZipCode;
        //                    //Destino
        //                    xlWorksheet.Cells[rowCount, 15] = destinataryName;
        //                    xlWorksheet.Cells[rowCount, 16] = destinataryCompany;
        //                    xlWorksheet.Cells[rowCount, 17] = destinaryPhonenumber;
        //                    xlWorksheet.Cells[rowCount, 18] = destinaryEmail;
        //                    xlWorksheet.Cells[rowCount, 19] = remitentCountry;
        //                    xlWorksheet.Cells[rowCount, 20] = remitentState;
        //                    xlWorksheet.Cells[rowCount, 21] = destinataryCity;
        //                    xlWorksheet.Cells[rowCount, 22] = destinaryBlock;
        //                    xlWorksheet.Cells[rowCount, 23] = destinaryStreet;
        //                    xlWorksheet.Cells[rowCount, 24] = destinaryNumber;
        //                    xlWorksheet.Cells[rowCount, 25] = destinataryZipCode;


        //                    xlWorksheet.Cells[rowCount, 26] = status;



        //                    Console.WriteLine("Guias procesadas = " + (rowCount - 2) + ", espere un momento por favor...");
        //                }
        //            }
        //            destinataryCity = "";
        //            destinataryCountry = "";
        //            destinataryState = "";
        //            destinataryZipCode = "";
        //            operatorName = "";
        //            remitentCity = "";
        //            remitentCompany = "";
        //            remitentCountry = "";
        //            remitentState = "";
        //            remitentZipCode = "";
        //            serviceName = "";
        //            status = "";
        //            tracking = "";
        //            trackingsList.Clear();
        //        }
        //        xlWorksheet.Cells[1, 1] = "Reporte de envios SONIA ALEJANDRA BRAUER VILLASEÑOR";
        //        xlWorksheet.Cells[2, 1] = "No. Solicitud"; //1
        //        xlWorksheet.Cells[2, 2] = "Guia";//2
        //        xlWorksheet.Cells[2, 3] = "Operador"; //3

        //        xlWorksheet.Cells[2, 4] = "Remitente";//4
        //        xlWorksheet.Cells[2, 5] = "Empresa del remitente";//5
        //        xlWorksheet.Cells[2, 6] = "Telefono del remitente";//5
        //        xlWorksheet.Cells[2, 7] = "Email del remitente";//5
        //        xlWorksheet.Cells[2, 8] = "Pais del remitente";//5
        //        xlWorksheet.Cells[2, 9] = "Estado del remitente";//5
        //        xlWorksheet.Cells[2, 10] = "Ciudad del remitente";//5
        //        xlWorksheet.Cells[2, 11] = "Colonia del remitente";//5
        //        xlWorksheet.Cells[2, 12] = "Calle del remitente";//5
        //        xlWorksheet.Cells[2, 13] = "Numero del remitente";//5
        //        xlWorksheet.Cells[2, 14] = "C.P. del remitente";//5

        //        xlWorksheet.Cells[2, 15] = "Destinatario";//6
        //        xlWorksheet.Cells[2, 16] = "Empresa del destinatario";//5
        //        xlWorksheet.Cells[2, 17] = "Telefono del destinatario";//5
        //        xlWorksheet.Cells[2, 18] = "Email del destinatario";//5
        //        xlWorksheet.Cells[2, 19] = "Pais del destinatario";//5
        //        xlWorksheet.Cells[2, 20] = "Estado del destinatario";//5
        //        xlWorksheet.Cells[2, 21] = "Ciudad del destinatario";//5
        //        xlWorksheet.Cells[2, 22] = "Colonia del destinatario";//5
        //        xlWorksheet.Cells[2, 23] = "Calle del destinatario";//
        //        xlWorksheet.Cells[2, 24] = "Numero del destinatario";//
        //        xlWorksheet.Cells[2, 25] = "C.P. del destinatario";//

        //        xlWorksheet.Cells[2, 26] = "Estatus del envio";//7

        //        Range xlRange = xlWorksheet.UsedRange;
        //        xlRange.EntireColumn.AutoFit();
        //        Borders xlBorder = xlRange.Borders;
        //        xlBorder.LineStyle = XlLineStyle.xlContinuous;
        //        xlBorder.Weight = 2d;
        //        Console.Clear();
        //        xlWorkbook.SaveAs("Reporte_SONIA_ALEJANDRA_BRAUER-" +
        //            DateTime.Today.Year.ToString() + "-" +
        //            DateTime.Today.Month.ToString() + "-" +
        //            DateTime.Today.Day.ToString());
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //        Marshal.ReleaseComObject(xlBorder);
        //        Marshal.ReleaseComObject(xlRange);
        //        Marshal.ReleaseComObject(xlWorksheet);
        //        xlWorkbook.Close();
        //        Marshal.ReleaseComObject(xlWorkbook);
        //        xlApp.Quit();
        //        Marshal.ReleaseComObject(xlApp);
        //        string name = "Reporte_SONIA_ALEJANDRA_BRAUER-" +
        //            DateTime.Today.Year.ToString() + "-" +
        //            DateTime.Today.Month.ToString() + "-" +
        //            DateTime.Today.Day.ToString();

        //        Email(name);
        //    }
        //    catch (SqlException e)
        //    {
        //        Console.WriteLine("Hubo un error al generar el reporte," +
        //            "\na continuacion se especifican los detalles del mismo:" +
        //            "\n\n" + e.ToString() +
        //            "\n\nFavor de contactar al administrador del sistema," +
        //            "\npara volver al menu principal favor de presionar la tecla Y...");
        //        while (Console.ReadKey(true).Key != ConsoleKey.Y)
        //        {

        //        }
        //    }
        //}

        public void CreateCustomReport()
        {
            List<string> JsonObjectsList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE FechaCreacion > '2021-06-07'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte Custom";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetCustomApiData(JsonObjectsList[i]);
                    rowCount++;
                    xlWorksheet.Cells[rowCount, 1] = ShipmentsList[i];
                    xlWorksheet.Cells[rowCount, 2].NumberFormat = "@";
                    xlWorksheet.Cells[rowCount, 2] = tracking;
                    xlWorksheet.Cells[rowCount, 4] = order;
                    xlWorksheet.Cells[rowCount, 5] = destinataryName;
                    Console.WriteLine("Guias procesadas = " + (rowCount - 2) + ", espere un momento por favor...");
                    tracking = "";
                    order = "";
                    destinataryName = "";
                }
                xlWorksheet.Cells[1, 1] = "Reporte Custom";
                xlWorksheet.Cells[2, 1] = "Numero de control";
                xlWorksheet.Cells[2, 2] = "Numero de guia";
                xlWorksheet.Cells[2, 3] = "Pedido";
                xlWorksheet.Cells[2, 4] = "Destinatario";
                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("ReporteCustom-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);

                string name = "ReporteCustom-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString();

                Email(name);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }

        }


        private static void SendSMTP(MailMessage message, string file)
        {
            SmtpClient smtpClient = new SmtpClient("smtp.sendgrid.net", Convert.ToInt32(587));
            System.Net.NetworkCredential credential = new System.Net.NetworkCredential("apikey", "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");
            smtpClient.Credentials = credential;





            // Create a message and set up the recipients.
            //MailMessage message = new MailMessage(
            //    "jane@contoso.com",
            //    "ben@contoso.com",
            //    "Quarterly data report.",
            //    "See the attached spreadsheet.");

            // Create  the file attachment for this email message.
            Attachment data = new Attachment(file, MediaTypeNames.Application.Octet);
            // Add time stamp information for the file.
            ContentDisposition disposition = data.ContentDisposition;
            disposition.CreationDate = System.IO.File.GetCreationTime(file);
            disposition.ModificationDate = System.IO.File.GetLastWriteTime(file);
            disposition.ReadDate = System.IO.File.GetLastAccessTime(file);
            // Add the file attachment to this email message.
            message.Attachments.Add(data);

            //Send the message.
            //SmtpClient client = new SmtpClient(server);
            //// Add credentials if the SMTP server requires them.
            //smtpClient.Credentials = CredentialCache.DefaultNetworkCredentials;

            try
            {
                smtpClient.Send(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught in CreateMessageWithAttachment(): {0}",
                    ex.ToString());
            }


            //System.Net.Mime.ContentType contentType = new System.Net.Mime.ContentType();
            //contentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Octet;
            //contentType.Name = "test.docx";
            //message.Attachments.Add(new Attachment(file, contentType));


           // smtpClient.Send(message);
        }



        public string Email( string name)
        {
            var succes = "Saludos.";
            string path = "C:\\Users\\ruben\\Documents\\" + name + ".xlsx";
            string correoOp = "";
            try
            {

                //ccEmailUserName02


                MailAddress from = new MailAddress("noreply@clickngo.com.mx", "noreply@clickngo.com.mx");


                correoOp = "rfuentes@thrimex.com";
                MailAddress to = new MailAddress(correoOp);
                List<MailAddress> listEmail = new List<MailAddress>();

                //listEmail.Add(
                //    new MailAddress("jpena@thrimex.com")
                //    ) ;
                //listEmail.Add(
                //new MailAddress("apena@carmarlogistics.com")
                //);
                //            listEmail.Add(
                //new MailAddress("vespinosa@clickngo.com.mx")
                //);
                //            listEmail.Add(
                //new MailAddress("rfuentes@thrimex.com")
                //);

                var message = new MailMessage(from, to);

                //foreach(var email in listEmail)
                //{
                //    message.CC.Add(email);
                //}
                //message.CC.Add(cc);
                //message.CC.Add(cc02);
                //message.CC.Add(cc03);


                message.IsBodyHtml = true;

                StringBuilder body = new StringBuilder();
                //body.AppendFormat(@"<img src='" + logoCnG + "' class='center'>");
                body.AppendFormat(@"Adjunto reporte de envios diario " + name);


                string saludo = @"Buen día,";

                string despedida = @"<br><br>Saludos.";


                string mensaje = saludo + body + despedida;

                DateTime serverTime = DateTime.Now; // gives you current Time in server timeZone
                DateTime utcTime = serverTime.ToUniversalTime(); // convert it to Utc using timezone setting of server computer
                TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time (Mexico)");
                DateTime localTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, tzi); // convert from utc to local
                                                                                    //string fecha = localTime.ToString("dd/MM/yyyy HH:mm:ss");


                message.Subject = "Reporte " + name;

                message.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(mensaje, null, MediaTypeNames.Text.Plain));
                message.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(mensaje, null, MediaTypeNames.Text.Html));

                SendSMTP(message, path);



            }
            catch (Exception ex)
            {
                succes = ex.Message;

            }
            return (succes);

        }

        public void CreatePenduloReport()
        {
            List<string> CreationDatesList = new List<string>();
            List<string> JsonObjectsList = new List<string>();
            List<string> ShipmentsList = new List<string>();
            try
            {
                SqlConnectionStringBuilder stringBuilderSql = new SqlConnectionStringBuilder()
                {
                    DataSource = "Traps-carmar.Database.windows.net",
                    UserID = "Thrimex",
                    Password = "Caradmin2018!",
                    InitialCatalog = "TRAPS-ADMIN"
                };
                using (SqlConnection connection = new SqlConnection(stringBuilderSql.ConnectionString))
                {
                    connection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("SELECT * ");
                    stringBuilder.Append("FROM PeticionApiEnvios ");
                    stringBuilder.Append("WHERE Status = 1 ");
                    stringBuilder.Append("AND NumeroControl LIKE 'CAFE%' ");
                    stringBuilder.Append("AND FechaCreacion > '2021-01-31'");
                    String stringResult = stringBuilder.ToString();
                    using (SqlCommand command = new SqlCommand(stringResult, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CreationDatesList.Add(reader[2].ToString());
                                JsonObjectsList.Add(reader[3].ToString());
                                ShipmentsList.Add(reader[6].ToString());
                            }
                        }
                    }
                    connection.Close();
                }
                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Add(Type.Missing);
                Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
                xlApp.Visible = false;
                xlApp.DisplayAlerts = false;
                xlWorksheet.Name = "Reporte de envios PENDULO";
                xlWorksheet.Cells.Font.Size = 12;
                int rowCount = 2;
                int longitudLista = ShipmentsList.Count;

                for (int i = 0; i < ShipmentsList.Count; i++)
                {
                    GetApiData("CAFEBRERIA EL PENDULO SA DE CV", JsonObjectsList[i], ShipmentsList[i]);
                    status = ChangeStatusLanguage(status);

                    if (shipmentType == 0)
                    {
                        if (trackingsList.Count > 0)
                        {
                            for (int j = 0; j < trackingsList.Count; j++)
                            {
                                rowCount++;
                                xlWorksheet.Cells[rowCount, 1] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                                xlWorksheet.Cells[rowCount, 2].NumberFormat = "@";
                                xlWorksheet.Cells[rowCount, 2] = trackingsList[j];
                                xlWorksheet.Cells[rowCount, 3] = destinataryName;
                                xlWorksheet.Cells[rowCount, 4] = destinataryCity;
                                xlWorksheet.Cells[rowCount, 5] = destinataryZipCode;
                                xlWorksheet.Cells[rowCount, 6] = destinataryState;
                                xlWorksheet.Cells[rowCount, 7] = status;
                                xlWorksheet.Cells[rowCount, 8] = heightsList[j];
                                xlWorksheet.Cells[rowCount, 9] = widthsList[j];
                                xlWorksheet.Cells[rowCount, 10] = lengthsList[j];
                                xlWorksheet.Cells[rowCount, 11] = weightsList[j];

                                Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                            }
                        }
                    }
                    else
                    {
                        if (tracking != "")
                        {
                            rowCount++;
                            xlWorksheet.Cells[rowCount, 1] = Convert.ToDateTime(CreationDatesList[i]).ToString("yyyy'-'MM'-'dd");
                            xlWorksheet.Cells[rowCount, 2].NumberFormat = "@";
                            xlWorksheet.Cells[rowCount, 2] = tracking;
                            xlWorksheet.Cells[rowCount, 3] = destinataryName;
                            xlWorksheet.Cells[rowCount, 4] = destinataryCity;
                            xlWorksheet.Cells[rowCount, 5] = destinataryZipCode;
                            xlWorksheet.Cells[rowCount, 6] = destinataryState;
                            xlWorksheet.Cells[rowCount, 7] = status;
                            xlWorksheet.Cells[rowCount, 8] = height;
                            xlWorksheet.Cells[rowCount, 9] = width;
                            xlWorksheet.Cells[rowCount, 10] = length;
                            xlWorksheet.Cells[rowCount, 11] = weight;


                            Console.WriteLine("Guias procesadas = " + (rowCount - 2) + " de " + longitudLista + ", espere un momento por favor...");
                        }
                    }

                    content = "";
                    destinataryCity = "";
                    destinataryName = "";
                    destinataryState = "";
                    destinataryZipCode = "";
                    height = 0;
                    heightsList.Clear();
                    length = 0;
                    lengthsList.Clear();
                    operatorName = "";
                    remitentCity = "";
                    remitentCompany = "";
                    remitentName = "";
                    remitentState = "";
                    remitentZipCode = "";
                    securedValue = 0;
                    securedValuesList.Clear();
                    serviceName = "";
                    status = "";
                    tracking = "";
                    trackingsList.Clear();
                    weight = 0;
                    weightsList.Clear();
                    width = 0;
                    widthsList.Clear();
                }
                xlWorksheet.Cells[1, 1] = "Reporte de envios CAFEBRERIA EL PENDULO SA DE CV";
                xlWorksheet.Cells[2, 1] = "Fecha del envio";
                xlWorksheet.Cells[2, 2] = "Numero de guia";
                xlWorksheet.Cells[2, 3] = "Nombre del destinatario";
                xlWorksheet.Cells[2, 4] = "Ciudad del destinatario";
                xlWorksheet.Cells[2, 5] = "Codigo postal del destinatario";
                xlWorksheet.Cells[2, 6] = "Estado del destinatario";
                xlWorksheet.Cells[2, 7] = "Estatus del envio";
                xlWorksheet.Cells[2, 8] = "Alto del envio";
                xlWorksheet.Cells[2, 9] = "Ancho del envio";
                xlWorksheet.Cells[2, 10] = "Largo del envio";
                xlWorksheet.Cells[2, 11] = "Peso del envio";

                Range xlRange = xlWorksheet.UsedRange;
                xlRange.EntireColumn.AutoFit();
                Borders xlBorder = xlRange.Borders;
                xlBorder.LineStyle = XlLineStyle.xlContinuous;
                xlBorder.Weight = 2d;
                Console.Clear();
                xlWorkbook.SaveAs("Reporte Pendulo-" +
                    DateTime.Today.Year.ToString() + "-" +
                    DateTime.Today.Month.ToString() + "-" +
                    DateTime.Today.Day.ToString());
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Marshal.ReleaseComObject(xlBorder);
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);


                string name = "Reporte Pendulo-" +
                DateTime.Today.Year.ToString() + "-" +
                DateTime.Today.Month.ToString() + "-" +
                DateTime.Today.Day.ToString();

                Email(name);


            }
            catch (SqlException e)
            {
                Console.WriteLine("Hubo un error al generar el reporte," +
                    "\na continuacion se especifican los detalles del mismo:" +
                    "\n\n" + e.ToString() +
                    "\n\nFavor de contactar al administrador del sistema," +
                    "\npara volver al menu principal favor de presionar la tecla Y...");
                while (Console.ReadKey(true).Key != ConsoleKey.Y)
                {

                }
            }




        }




    }

}
